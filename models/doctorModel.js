const mongooose = require('mongoose');
const doctorSchema = new mongooose.Schema(
  {
    doctor_name: {
      type: String,
      required: true,
    },
    speciality: {
      type: String,
      required: true,
    },
    nmc_number: {
      type: String,

      required: true,
    },
    contact_number: [{ type: Number }],
    location: [{ type: Number }],
    rating: { type: Number },
    doctor_img: { type: String },
  },
  {
    timestamps: true,
  }
);

const doctorModel = mongooose.model('doctors', doctorSchema);
module.exports = doctorModel;
