const mongoose = require('mongoose');
const icuSchema = new mongoose.Schema(
  {
    icu_name: {
      type: String,
      required: true,
    },
    location: [{ type: Number }],
    total_beds: [{ type: Number, required: true }],
    contact_number: [{ type: Number, required: true }],
    icu_img: {
      type: String,
    },
    price: {
      type: Number,
    },
    public_id_cloudinary: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

const icuModel = mongoose.model('icu', icuSchema);
module.exports = icuModel;
