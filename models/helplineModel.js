const mongooose = require('mongoose');
const helpLineSchema = new mongooose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    contact_number: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);

const helpModel = mongooose.model('helpline', helpLineSchema);
module.exports = helpModel;
