const mongoose = require('mongoose');
const hospitalSchema = new mongoose.Schema(
  {
    hospital_name: {
      type: String,
      required: true,
    },
    location: [{ type: Number }],
    services: [{ type: String }],
    contact_number: [{ type: Number }],
    hospital_img: {
      type: String,
    },
    rating: {
      type: Number
    
    }
  },
  {
    timestamps: true,
  }
);

const hospitalModel = mongoose.model('hospitals', hospitalSchema);
module.exports = hospitalModel;
