const mongooose = require('mongoose');
const userSchema = new mongooose.Schema(
  {
    username: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    role: {
      type: Number,
      default: 1,
      required: true, //1 for admin and 2 for normal user
    },
  },
  {
    timestamps: true,
  }
);

const userModel = mongooose.model('users', userSchema);
module.exports = userModel;
