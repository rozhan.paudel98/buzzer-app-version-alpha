const mongooose = require('mongoose');
const bloodBankSchema = new mongooose.Schema(
  {
    bloodbank_name: {
      type: String,
      required: true,
    },
    contact_number: [{ type: Number }],
    location: [{ type: Number }],
  },
  {
    timestamps: true,
  }
);

const bloodModel = mongooose.model('bloodbank', bloodBankSchema);
module.exports = bloodModel;
