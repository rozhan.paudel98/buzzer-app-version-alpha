const mongoose = require('mongoose');
const pharmacySchema = new mongoose.Schema(
  {
    pharmacy_name: {
      type: String,
      required: true,
    },
    location: [{ type: Number }],
    hour_open_range: {
      type: String,
    },
    contact_number: [{ type: Number }],
    pharmacy_img: {
      type: String,
    },
    public_id_cloudinary: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

const PharmacyModel = mongoose.model('pharmacy', pharmacySchema);
module.exports = PharmacyModel;
