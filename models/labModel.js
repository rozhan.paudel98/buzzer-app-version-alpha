const mongoose = require('mongoose');
const labSchema = new mongoose.Schema(
  {
    lab_name: {
      type: String,
      required: true,
    },
    location: [{ type: Number }],
    services: [{ type: String }],
    contact_number: [{ type: Number }],
    lab_img: {
      type: String,
    },
    public_id_cloudinary: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

const labModel = mongoose.model('labs', labSchema);
module.exports = labModel;
