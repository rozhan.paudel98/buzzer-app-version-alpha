const mongooose = require('mongoose');
const ambulanceSchema = new mongooose.Schema(
  {
    ambulance_name: {
      type: String,
      required: true,
    },
    contact_number: [{ type: Number }],
    location: [{ type: Number }],
    rate: { type: Number },
    ambulance_img: { type: String },
  },
  {
    timestamps: true,
  }
);

const ambulanceModel = mongooose.model('ambulance', ambulanceSchema);
module.exports = ambulanceModel;
