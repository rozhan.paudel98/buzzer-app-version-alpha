var express = require('express');
var router = express.Router();
var { computeDistanceBetween, LatLng } = require('spherical-geometry-js');
const { check, validationResult } = require('express-validator/check');
const auth = require('../middlewares/auth');
var bloodModel = require('../models/boodBankModel');
var userModel = require('../models/userModel');

require('dotenv').config();

// GET ALL REGISTERED Blood Banks
router.get('/', function (req, res, next) {
  bloodModel.find().then(function (error, done) {
    if (error) return res.send(error);
    else {
      res.json({
        msg: 'Fetch Success y',
        bloodBanks: done,
      });
    }
  });
});

//ADD Blood Bank
router.post(
  '/add',
  auth,
  [
    check('bloodbank_name', 'bloodbank_name field is required').not().isEmpty(),

    check('location', 'Location is requierd').not().isEmpty(),

    check('contact_number', 'contact_number is requierd').not().isEmpty(),
  ],
  function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({
        errors: errors.array(),
      });
    } else {
      //check if user is admin
      userModel.findById({ _id: req.user.user.id }).exec(function (err, user) {
        if (err) return res.json({ msg: 'Server Error here !', error: err });
        //if user role is not admin
        if (user) {
          if (user.role !== 1) {
            return res.json({
              msg: 'unauthorized Access',
            });
          } else {
            //CHECK IF Blood Bank EXISTS
            bloodModel
              .findOne({ bloodbank_name: req.body.bloodbank_name })
              .exec(function (error, bloodbank) {
                if (error)
                  next({
                    msg: 'Server ERROR',
                  });
                if (bloodbank) {
                  return res.json({
                    msg: 'Blood bank already Exists',
                  });
                } else {
                  if (!bloodbank) {
                    //Creating a new ambulance
                    const newBloodBank = new bloodModel({
                      bloodbank_name: req.body.bloodbank_name,
                    });

                    //Split number by comma and add it to new bloodbank

                    var numbers = req.body.contact_number.split(',');
                    numbers.forEach((number) => {
                      newBloodBank.contact_number.push(number);
                    });

                    //Split Location by comma and pushing it to new blood bank
                    var loc_points = req.body.location.split(',');
                    loc_points.forEach((point) => {
                      newBloodBank.location.push(point);
                    });

                    //Saving newBlood Bank

                    newBloodBank.save(function (error, result) {
                      if (error)
                        return res.json({
                          msg: 'Server Error',
                          error: error,
                        });
                      if (result) {
                        res.json({
                          msg: 'Blood Bank Added',
                          bloodbank: result,
                        });
                      }
                    });
                  }
                }
              });
          }
        }
      });
    }
  }
);

//Filter GET ROUTES

//GET Nearby Ambulance WITH User Location  PARAMETER

router.get(
  '/find-nearby',
  auth,
  [
    check('latitude', 'latitude is required and it should be number')
      .not()
      .isEmpty()
      .isNumeric(),

    check('longitude', 'Longitude is required and it should be number')
      .not()
      .isEmpty()
      .isNumeric(),
  ],
  function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({
        errors: errors.array(),
      });
    } else {
      bloodModel.find().exec(function (error, bloodBanks) {
        if (error) return res.send(error);
        else {
          var nearBloodBanks = [];
          var lat = Number(req.body.latitude);
          var lon = Number(req.body.longitude);

          //using latlng initializer

          var d1 = new LatLng(lat, lon);

          bloodBanks.forEach((bloodBank) => {
            //using latlng init

            var d2 = new LatLng(
              Number(bloodBank.location[0]),
              Number(bloodBank.location[1])
            );

            //computing distance between two latlng

            var distance = computeDistanceBetween(d1, d2);
            var obj = {
              bloodbank_name: bloodBank.bloodbank_name,
              contact_number: bloodBank.contact_number,
              location: bloodBank.location,

              distance_in_meter: distance,
            };
            nearBloodBanks.push(obj);
          });

          //Splice to copy nearhospitals

          var byKm = nearBloodBanks.slice(0);

          //sorting by lowest distance first

          byKm.sort(function (a, b) {
            return a.Distance_in_meter - b.Distance_in_meter;
          });

          res.json({
            msg: 'Success ! Blood Banks according to lowest distance first',
            data: byKm,
          });
        }
      });
    }
  }
);

//Filter Get routes

// Update Blood Bank by id

router.put('/add/:id', auth, function (req, res, next) {
  //Creating Empty Object for inserting update
  var update = {};
  //mapping it to update
  if (req.body.bloodbank_name) update.bloodbank_name = req.body.bloodbank_name;

  //Split number by comma and add it to update
  if (req.body.contact_number) {
    var newContacts = [];
    var numbers = req.body.contact_number.split(',');
    numbers.forEach((number) => {
      newContacts.push(number);
    });
    update.contact_number = newContacts;
  }

  if (req.body.location) {
    //Split Location by comma and pushing it to update
    var loc_ = [];
    var loc_points = req.body.location.split(',');
    loc_points.forEach((point) => {
      loc_.push(point);
    });
    update.location = loc_;
  }

  bloodModel
    .findByIdAndUpdate({ _id: req.params.id }, update)
    .exec(function (error, done) {
      if (error)
        return res.json({
          msg: 'server error',
          error: error,
        });
      else {
        res.json({
          msg: 'Updated !!',
          data: done,
        });
      }
    });
});

// Delete Blood Bank by id
router.delete('/:id', auth, function (req, res, next) {
  bloodModel.findByIdAndDelete({ _id: req.params.id }, function (error, done) {
    if (error) return res.send(error);
    else {
      res.json({
        msg: 'Blood Bank Deleted !',
        data: done,
      });
    }
  });
});

module.exports = router;
