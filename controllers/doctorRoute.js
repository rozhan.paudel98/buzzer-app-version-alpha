var express = require('express');
var router = express.Router();
var cloudinary = require('cloudinary');
var multer = require('multer');
var { computeDistanceBetween, LatLng } = require('spherical-geometry-js');
const { check, validationResult } = require('express-validator/check');
const myStorage = require('../configs/multerSetup');
const auth = require('../middlewares/auth');
var doctorModel = require('../models/doctorModel');
var userModel = require('../models/userModel');

require('dotenv').config();

// CLoudinary Config
require('../configs/cloudinary');

// Multer Storage Setup
var upload = multer({
  storage: myStorage,
});

// GET ALL REGISTERED DOCTORS
router.get('/', function (req, res, next) {
  doctorModel.find().then(function (error, done) {
    if (error) return res.send(error);
    else {
      res.json({
        msg: 'Fetch Success',
        doctors: done,
      });
    }
  });
});

//ADD Doctors
router.post(
  '/add',
  auth,
  upload.single('doctor_img'),

  function (req, res, next) {
    //check if user is admin
    userModel.findById({ _id: req.user.user.id }).exec(function (err, user) {
      if (err) return res.json({ msg: 'Server Error !', error: err });
      //if user role is not admin
      if (user) {
        if (user.role !== 1) {
          return res.json({
            msg: 'unauthorized Access',
          });
        } else {
          //CHECK IF Doctor EXISTS
          doctorModel
            .findOne({ nmc_number: req.body.nmc_number })
            .exec(function (error, doctor) {
              if (error)
                next({
                  msg: 'Server ERROR',
                });
              if (doctor) {
                return res.json({
                  msg: 'Doctor already Exists',
                });
              } else {
                //CLoudinary upload
                const result = cloudinary.v2.uploader.upload(req.file.path);

                result.then((success) => {
                  if (!doctor) {
                    //Creating a new Hospital
                    const newDoctor = new doctorModel({
                      doctor_name: req.body.doctor_name,
                      doctor_img: success.url,
                      rating: req.body.rating,
                      nmc_number: req.body.nmc_number,
                      speciality: req.body.speciality,
                    });

                    //Split number by comma and add it to new Doctor

                    var numbers = req.body.contact_number.split(',');
                    numbers.forEach((number) => {
                      newDoctor.contact_number.push(number);
                    });

                    //Split Location by comma and pushing it to new Doctor
                    var loc_points = req.body.location.split(',');
                    loc_points.forEach((point) => {
                      newDoctor.location.push(point);
                    });

                    //Saving newDoctor

                    newDoctor.save(function (error, result) {
                      if (error)
                        return res.json({
                          msg: 'Server Error',
                          error: error,
                        });
                      if (result) {
                        res.json({
                          msg: 'Doctor Added',
                          doctor: result,
                        });
                      }
                    });
                  }
                });
              }
            });
        }
      }
    });
  }
);

//Filter GET ROUTES

// GET ALL Doctors Speciality only
router.get('/speciality-only', auth, function (req, res, next) {
  doctorModel.find().exec(function (error, doctors) {
    if (error) return res.send(error);
    else {
      var speciality = [];
      doctors.forEach((doctor) => {
        if (speciality.indexOf(doctor.speciality) == -1)
          //Check if speciality exits in array
          speciality.push(doctor.speciality);
      });
      res.json({
        msg: 'Success',
        data: speciality,
      });
    }
  });
});

//GET DOCTOR WITH SPECIALITY PARAMETER

router.get(
  '/speciality',
  auth,
  [
    check('latitude', 'latitude is required and it should be number')
      .not()
      .isEmpty()
      .isNumeric(),

    check('longitude', 'Longitude is required and it should be number')
      .not()
      .isEmpty()
      .isNumeric(),
  ],
  function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({
        errors: errors.array(),
      });
    } else {
      doctorModel.find().exec(function (error, doctors) {
        if (error) return res.send(error);
        else {
          var nearDoctors = [];
          var lat = Number(req.body.latitude);
          var lon = Number(req.body.longitude);

          //using latlng initializer

          var d1 = new LatLng(lat, lon);

          doctors.forEach((doctor) => {
            //using latlng init

            var d2 = new LatLng(
              Number(doctor.location[0]),
              Number(doctor.location[1])
            );

            //computing distance between two latlng

            var distance = computeDistanceBetween(d1, d2);
            var obj = {
              doctor_name: doctor.doctor_name,
              speciality: doctor.speciality,
              nmc_number: doctor.nmc_number,
              contact_number: doctor.contact_number,
              location: doctor.location,
              rating: doctor.rating,
              distance_in_meter: distance,
              doctor_img: doctor.doctor_img,
            };
            nearDoctors.push(obj);
          });

          //Splice to copy nearhospitals

          var byKm = nearDoctors.slice(0);

          //sorting by lowest distance first

          byKm.sort(function (a, b) {
            return a.Distance_in_meter - b.Distance_in_meter;
          });

          if (req.body.speciality) {
            if (req.body.speciality == 'all') {
              return res.json({
                msg: 'success, All Doctors with Diffrent Speciality',
                data: byKm,
              });
            } else {
              const filtered_docs = byKm.filter((doctor) => {
                return (
                  doctor.speciality.toLowerCase() ==
                  req.body.speciality.toLowerCase()
                );
              });

              return res.json({
                msg: 'Success ! Filtered Doctors',
                data: filtered_docs,
              });
            }
          } else {
            res.json({
              msg: ' success, All Doctors with Diffrent Speciality',
              data: byKm,
            });
          }
        }
      });
    }
  }
);

//Filter Get routes

// Update Doctor by id

router.put('/add/:id', auth, function (req, res, next) {
  //Creating Empty Object for inserting update
  var update = {};
  //mapping it to update
  if (req.body.doctor_name) update.doctor_name = req.body.doctor_name;
  if (req.body.nmc_number) update.nmc_number = req.body.nmc_number;
  if (req.body.speciality) update.speciality = req.body.speciality;
  if (req.body.rating) update.rating = req.body.rating;

  //Split number by comma and add it to update
  if (req.body.contact_number) {
    var newContacts = [];
    var numbers = req.body.contact_number.split(',');
    numbers.forEach((number) => {
      newContacts.push(number);
    });
    update.contact_number = newContacts;
  }

  if (req.body.location) {
    //Split Location by comma and pushing it to update
    var loc_ = [];
    var loc_points = req.body.location.split(',');
    loc_points.forEach((point) => {
      loc_.push(point);
    });
    update.location = loc_;
  }

  doctorModel
    .findByIdAndUpdate({ _id: req.params.id }, update)
    .exec(function (error, done) {
      if (error)
        return res.json({
          msg: 'server error',
          error: error,
        });
      else {
        res.json({
          msg: 'Updated !!',
          data: done,
        });
      }
    });
});

// Delete Doctor by id
router.delete('/:id', auth, function (req, res, next) {
  doctorModel.findByIdAndDelete({ _id: req.params.id }, function (error, done) {
    if (error) return res.send(error);
    else {
      res.json({
        msg: 'Doctor Deleted !',
        data: done,
      });
    }
  });
});

module.exports = router;
