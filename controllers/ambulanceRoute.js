var express = require('express');
var router = express.Router();
var cloudinary = require('cloudinary');
var multer = require('multer');
var { computeDistanceBetween, LatLng } = require('spherical-geometry-js');
const { check, validationResult } = require('express-validator/check');
const myStorage = require('../configs/multerSetup');
const auth = require('../middlewares/auth');
var ambulanceModel = require('../models/ambulanceModel');
var userModel = require('../models/userModel');

require('dotenv').config();

// CLoudinary Config
require('../configs/cloudinary');

// Multer Storage Setup
var upload = multer({
  storage: myStorage,
});

// GET ALL REGISTERED Ambulance
router.get('/', function (req, res, next) {
  ambulanceModel.find().then(function (error, done) {
    if (error) return res.send(error);
    else {
      res.json({
        msg: 'Fetch Success',
        doctors: done,
      });
    }
  });
});

//ADD Ambulance
router.post(
  '/add',
  auth,
  upload.single('ambulance_img'),

  function (req, res, next) {
    //check if user is admin
    userModel.findById({ _id: req.user.user.id }).exec(function (err, user) {
      if (err) return res.json({ msg: 'Server Error !', error: err });
      //if user role is not admin
      if (user) {
        if (user.role !== 1) {
          return res.json({
            msg: 'unauthorized Access',
          });
        } else {
          //CHECK IF Ambulance EXISTS
          ambulanceModel
            .findOne({ ambulance_name: req.body.ambulance_name })
            .exec(function (error, ambulance) {
              if (error)
                next({
                  msg: 'Server ERROR',
                });
              if (ambulance) {
                return res.json({
                  msg: 'Ambulance already Exists',
                });
              } else {
                //CLoudinary upload
                const result = cloudinary.v2.uploader.upload(req.file.path);

                result.then((success) => {
                  if (!ambulance) {
                    //Creating a new ambulance
                    const newAmbulance = new ambulanceModel({
                      ambulance_name: req.body.ambulance_name,
                      ambulance_img: success.url,
                      rate: Number(req.body.rate),
                    });

                    //Split number by comma and add it to new Ambulance

                    var numbers = req.body.contact_number.split(',');
                    numbers.forEach((number) => {
                      newAmbulance.contact_number.push(number);
                    });

                    //Split Location by comma and pushing it to new Ambulance
                    var loc_points = req.body.location.split(',');
                    loc_points.forEach((point) => {
                      newAmbulance.location.push(point);
                    });

                    //Saving newAmbulance

                    newAmbulance.save(function (error, result) {
                      if (error)
                        return res.json({
                          msg: 'Server Error',
                          error: error,
                        });
                      if (result) {
                        res.json({
                          msg: 'Ambulance Added',
                          Ambulance: result,
                        });
                      }
                    });
                  }
                });
              }
            });
        }
      }
    });
  }
);

//Filter GET ROUTES

//GET Nearby Ambulance WITH User Location  PARAMETER

router.get(
  '/find-nearby',
  auth,
  [
    check('latitude', 'latitude is required and it should be number')
      .not()
      .isEmpty()
      .isNumeric(),

    check('longitude', 'Longitude is required and it should be number')
      .not()
      .isEmpty()
      .isNumeric(),
  ],
  function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({
        errors: errors.array(),
      });
    } else {
      ambulanceModel.find().exec(function (error, ambulances) {
        if (error) return res.send(error);
        else {
          var nearAmbulances = [];
          var lat = Number(req.body.latitude);
          var lon = Number(req.body.longitude);

          //using latlng initializer

          var d1 = new LatLng(lat, lon);

          ambulances.forEach((ambulance) => {
            //using latlng init

            var d2 = new LatLng(
              Number(ambulance.location[0]),
              Number(ambulance.location[1])
            );

            //computing distance between two latlng

            var distance = computeDistanceBetween(d1, d2);
            var obj = {
              ambulance_name: ambulance.ambulance_name,
              contact_number: ambulance.contact_number,
              location: ambulance.location,
              rate: ambulance.rate,
              distance_in_meter: distance,
              ambulance_img: ambulance.ambulance_img,
            };
            nearAmbulances.push(obj);
          });

          //Splice to copy nearhospitals

          var byKm = nearAmbulances.slice(0);

          //sorting by lowest distance first

          byKm.sort(function (a, b) {
            return a.Distance_in_meter - b.Distance_in_meter;
          });

          res.json({
            msg: 'Success ! Ambulances according to lowest distance first',
            data: byKm,
          });
        }
      });
    }
  }
);

//Filter Get routes

// Update Doctor by id

router.put('/add/:id', auth, function (req, res, next) {
  //Creating Empty Object for inserting update
  var update = {};
  //mapping it to update
  if (req.body.ambulance_name) update.ambulance_name = req.body.ambulance_name;
  if (req.body.rate) update.rate = Number(req.body.rate);

  //Split number by comma and add it to update
  if (req.body.contact_number) {
    var newContacts = [];
    var numbers = req.body.contact_number.split(',');
    numbers.forEach((number) => {
      newContacts.push(number);
    });
    update.contact_number = newContacts;
  }

  if (req.body.location) {
    //Split Location by comma and pushing it to update
    var loc_ = [];
    var loc_points = req.body.location.split(',');
    loc_points.forEach((point) => {
      loc_.push(point);
    });
    update.location = loc_;
  }

  ambulanceModel
    .findByIdAndUpdate({ _id: req.params.id }, update)
    .exec(function (error, done) {
      if (error)
        return res.json({
          msg: 'server error',
          error: error,
        });
      else {
        res.json({
          msg: 'Updated !!',
          data: done,
        });
      }
    });
});

// Delete Ambulance by id
router.delete('/:id', auth, function (req, res, next) {
  ambulanceModel.findByIdAndDelete({ _id: req.params.id }, function (
    error,
    done
  ) {
    if (error) return res.send(error);
    else {
      res.json({
        msg: 'Ambulance Deleted !',
        data: done,
      });
    }
  });
});

module.exports = router;
