var express = require('express');
var router = express.Router();
var cloudinary = require('cloudinary');
var multer = require('multer');
const { check, validationResult } = require('express-validator/check');
const { headingDistanceTo, toLatLon } = require('geolocation-utils');
const PharmacyModel = require('../models/pharmacyModel');
const myStorage = require('../configs/multerSetup');
const auth = require('../middlewares/auth');
const userModel = require('../models/userModel');

require('dotenv').config();

// CLoudinary Config
require('../configs/cloudinary');

// Multer Storage Setup
var upload = multer({
  storage: myStorage,
});

//GET ALL Pharmacy
router.get('/', auth, function (req, res, next) {
  PharmacyModel.find().exec(function (error, pharmacy) {
    if (error) return res.status(500).json({ msg: 'Server Error !' });
    if (!pharmacy)
      res.json({
        msg: 'No Pharmacy Found',
      });
    res.status(200).json({
      data: pharmacy,
    });
  });
});

//Find NearBy Pharmacies
router.get('/find-nearby', auth, function (req, res, next) {
  PharmacyModel.find().exec(function (error, pharmacies) {
    if (error) return res.json({ msg: 'Server Error !', error: error });
    if (!pharmacies)
      return res.json({
        msg: 'No Pharmacy Found',
      });
    else {
      var nearPharmacy = [];
      var latitude = Number(req.body.latitude);
      var longitude = Number(req.body.longitude);

      var d1 = toLatLon([latitude, longitude]);

      pharmacies.forEach((lab) => {
        //using latlng init

        var d2 = toLatLon([Number(lab.location[0]), Number(lab.location[1])]);

        //computing distance between two latlng

        var distanceinm = headingDistanceTo(d1, d2);
        console.log(distanceinm);
        var obj = {
          pharmacy_name: lab.pharmacy_name,
          contact_number: lab.contact_number,
          location: lab.location,
          hour_open_range: lab.hour_open_range,
          pharmacy_img: lab.pharmacy_img,
          Distance_in_meter: distanceinm.distance,
        };
        nearPharmacy.push(obj);
      });

      //Slice to copy nearhospitals

      var byKm = nearPharmacy.slice(0);

      //sorting by lowest distance first

      byKm.sort(function (a, b) {
        return a.Distance_in_meter - b.Distance_in_meter;
      });
      return res.json({
        byKm,
      });
    }
  });
});

//ADD Pharmacy
router.post(
  '/add',
  auth,
  upload.single('pharmacy_img'),
  [
    check('pharmacy_name', 'Pharmacy Name is required').not().isEmpty(),

    check('location', 'Location is required').not().isEmpty(),
    check('hour_open_range', 'Open Hours Range is required').not().isEmpty(),
    check('contact_number', 'Contact Number is required').not().isEmpty(),
  ],
  function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({
        errors: errors.array(),
      });
    } else {
      //check if user is admin
      userModel.findById({ _id: req.user.user.id }).exec(function (err, user) {
        if (err) return res.json({ msg: 'Server Error !' });
        //if user role is not admin
        if (user) {
          if (user.role !== 1) {
            return res.json({
              msg: 'unauthorized Access',
            });
          } else {
            //CHECK IF Pharmacy EXISTS
            PharmacyModel.findOne({
              pharmacy_name: req.body.pharmacy_name,
            }).exec(function (error, pharmacy) {
              if (error)
                return res.json({
                  msg: 'Server ERROR',
                });
              if (pharmacy) {
                return res.json({
                  msg: 'Pharmacy already Exists',
                });
              } else {
                //CLoudinary upload
                const result = cloudinary.v2.uploader.upload(req.file.path);
                result.then((success) => {
                  if (!pharmacy) {
                    //Creating a new Lab
                    const newPharmacy = new PharmacyModel({
                      pharmacy_name: req.body.pharmacy_name,
                      hour_open_range: req.body.hour_open_range,
                      pharmacy_img: success.url,
                      public_id_cloudinary: success.public_id,
                    });

                    //Split number by comma

                    var numbers = req.body.contact_number.split(',');
                    numbers.forEach((number) => {
                      newPharmacy.contact_number.push(number);
                    });

                    //Split Location by comma
                    var loc_points = req.body.location.split(',');
                    loc_points.forEach((point) => {
                      newPharmacy.location.push(point);
                    });

                    //Saving new Pharmacy

                    newPharmacy.save(function (error, done) {
                      if (error)
                        next({
                          msg: 'Server Error',
                        });
                      if (done) {
                        res.json({
                          msg: 'Pharmacy Added',
                          pharmacy: done,
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        }
      });
    }
  }
);

//MODIFY Pharmacy DETAILS

router.put('/add/:id', auth, function (req, res, next) {
  //check if user is admin
  userModel.findById({ _id: req.user.user.id }).exec(function (err, user) {
    if (err) return res.json({ msg: 'Server Error !' });
    //if user role is not admin
    if (user) {
      if (user.role !== 1) {
        return res.json({
          msg: 'unauthorized Access',
        });
      } else {
        //Creating Empty Object for inserting update
        var update = {};
        //mapping it to update
        if (req.body.pharmacy_name)
          update.pharmacy_name = req.body.pharmacy_name;

        //Split number by comma and add it to update
        if (req.body.contact_number) {
          var newContacts = [];
          var numbers = req.body.contact_number.split(',');
          numbers.forEach((number) => {
            newContacts.push(number);
          });
          update.contact_number = newContacts;
        }

        if (req.body.location) {
          //Split Location by comma and pushing it to update
          var loc_ = [];
          var loc_points = req.body.location.split(',');
          loc_points.forEach((point) => {
            loc_.push(point);
          });
          update.location = loc_;
        }

        if (req.body.hour_open_range)
          update.hour_open_range = req.body.hour_open_range;

        PharmacyModel.findByIdAndUpdate({ _id: req.params.id }, update).exec(
          function (error, done) {
            if (error)
              return res.json({
                msg: 'server error',
                error: error,
              });
            else {
              res.json({
                msg: 'Updated !!',
                data: done,
              });
            }
          }
        );
      }
    }
  });
});

//DELETE Pharmacy BY ID

router.delete('/:id', auth, function (req, res, next) {
  PharmacyModel.findOneAndDelete({ _id: req.params.id }).exec(function (
    error,
    pharmacy
  ) {
    if (error) return res.status(500).json({ msg: 'Server Error !' });
    if (!pharmacy)
      return res.json({
        msg: 'Pharmacy doesnot exist',
      });
    else {
      cloudinary.v2.uploader.destroy(pharmacy.public_id_cloudinary, function (
        err,
        done
      ) {
        if (err) return res.send(err);
        else {
          res.status(200).json({
            msg: 'Deleted !',
            data: pharmacy,
          });
        }
      });
    }
  });
});

module.exports = router;
