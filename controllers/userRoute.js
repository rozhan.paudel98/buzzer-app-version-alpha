var express = require('express');
var router = express.Router();
const userModel = require('../models/userModel');
const { check, validationResult } = require('express-validator/check');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { jwtSecret } = require('../configs/secrets');

router.get('/', function (req, res, next) {
  res.send('User/ route');
});

//Register Users

router.post(
  '/',
  [
    check('username', 'username is required').not().isEmpty(),

    check('password', 'Password is required').not().isEmpty(),
  ],
  async function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
     return res.json({
        errors: errors.array(),
      });
    } else {
      try {
        //CHECK IF USER EXISTS
        await userModel
          .findOne({ username: req.body.username })
          .exec(function (error, user) {
            if (user) {
              return res.json({
                 msg: 'User already Exists' 
              });
            }
          });

        //creating new user

        const newUser = new userModel({
          username: req.body.username,
          password: req.body.password,
          role: req.body.role,
        });

        //encrypting the password

        const salt = await bcrypt.genSalt(10);
        const hashedPW = await bcrypt.hash(req.body.password, salt);
        newUser.password = hashedPW;

        //Saving the newuser

        newUser.save(function (error, user) {
          if (error) throw error;
          if (user) {
            //Creating Payload Object
            const payload = {
              user: {
                id: user.id,
              },
            };

            //Generating JWT TOKEN
            const jwtSecret = process.env.JWT_SECRET;
            jwt.sign(payload, jwtSecret, { expiresIn: 36000 }, function (
              error,
              token
            ) {
              res.status(200).json({
                msg:"Registered !",
                user: user,
                token: token,
              });
            });
          }
        });
      } catch (error) {
        res.status(500).json({
          error,
        });
      }
    }
  }
);

//LOGIN ROUTE
router.post(
  '/login',
  [
    check('username', 'Valid username is required').not(),
    check('password', 'Password is requierd').not(),
  ],
  async function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({
        errors: errors.array(),
      });
    }

    //finding the user
    await userModel
      .findOne({ username: req.body.username })
      .exec(function (error, user) {
        if (error) return next(error);
        if (!user) return res.json({ msg: 'Invalid Credentials' });

        const isMatch = bcrypt.compare(
          req.body.password,
          user.password,
          function (error, done) {
            if (error) next(error);
            if (done == true) {
              const payload = {
                user: {
                  id: user.id,
                },
              };

              jwt.sign(payload, jwtSecret, { expiresIn: 36000 }, function (
                error,
                token
              ) {
                if (error) next(error);

                res.json({ token });
              });
            } else {
              res.status(401).json({
                msg: 'Invalid Credentials',
              });
            }
          }
        );
      });
  }
);

module.exports = router;
