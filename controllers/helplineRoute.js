var express = require('express');
var router = express.Router();
const { check, validationResult } = require('express-validator/check');
const auth = require('../middlewares/auth');
var helpModel = require('../models/helplineModel');
var userModel = require('../models/userModel');

require('dotenv').config();

// GET ALL REGISTERED Help line Numbers
router.get('/', function (req, res, next) {
  helpModel.find().exec(function (error, done) {
    if (error) return res.send(error);
    else {
      res.json({
        msg: 'Fetch Success ',
        data: done,
      });
    }
  });
});

//ADD HelpLine Number
router.post(
  '/add',
  auth,
  [
    check('title', 'Title field is required').not().isEmpty(),
    check('contact_number', 'contact Number is required').not().isEmpty(),
  ],
  function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({
        errors: errors.array(),
      });
    } else {
      //check if user is admin
      userModel.findById({ _id: req.user.user.id }).exec(function (err, user) {
        if (err) return res.json({ msg: 'Server Error here !', error: err });
        //if user role is not admin
        if (user) {
          if (user.role !== 1) {
            return res.json({
              msg: 'unauthorized Access',
            });
          } else {
            //CHECK IF helpline Number EXISTS
            helpModel
              .findOne({ title: req.body.title })
              .exec(function (error, data) {
                if (error)
                  next({
                    msg: 'Server ERROR',
                  });
                if (data) {
                  return res.json({
                    msg: 'Helpline with input title already Exists',
                  });
                } else {
                  if (!data) {
                    //Creating a new new helpline
                    const newHelpLine = new helpModel({
                      title: req.body.title,
                      contact_number: req.body.contact_number,
                    });

                    //Saving

                    newHelpLine.save(function (error, result) {
                      if (error)
                        return res.json({
                          msg: 'Server Error',
                          error: error,
                        });
                      if (result) {
                        res.json({
                          msg: 'Helpline Added',
                          data: result,
                        });
                      }
                    });
                  }
                }
              });
          }
        }
      });
    }
  }
);

// Update Helpline by id

router.put('/add/:id', auth, function (req, res, next) {
  //Creating Empty Object for inserting update
  var update = {};
  //mapping it to update
  if (req.body.title) update.title = req.body.title;
  if (req.body.contact_number) update.contact_number = req.body.contact_number;

  helpModel
    .findByIdAndUpdate({ _id: req.params.id }, update)
    .exec(function (error, done) {
      if (error)
        return res.json({
          msg: 'server error',
          error: error,
        });
      else {
        res.json({
          msg: 'Updated !!',
          data: done,
        });
      }
    });
});

// Delete helpline by id
router.delete('/:id', auth, function (req, res, next) {
  helpModel.findByIdAndDelete({ _id: req.params.id }, function (error, done) {
    if (error) return res.send(error);
    else {
      res.json({
        msg: 'Helpline Deleted !',
        data: done,
      });
    }
  });
});

module.exports = router;
