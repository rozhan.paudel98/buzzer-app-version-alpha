var express = require('express');
var router = express.Router();
var cloudinary = require('cloudinary');
var multer = require('multer');
var {
  computeDistanceBetween,
  LatLng,
  convertLatLng,
} = require('spherical-geometry-js');
const { check, validationResult } = require('express-validator/check');
const { headingDistanceTo, toLatLon } = require('geolocation-utils');
const labModel = require('../models/labModel.js');
const myStorage = require('../configs/multerSetup');
const auth = require('../middlewares/auth');
const userModel = require('../models/userModel');

require('dotenv').config();

// CLoudinary Config
require('../configs/cloudinary');

// Multer Storage Setup
var upload = multer({
  storage: myStorage,
});

//GET ALL Labs
router.get('/', auth, function (req, res, next) {
  labModel.find().exec(function (error, labs) {
    if (error) return res.status(500).json({ msg: 'Server Error !' });
    if (!labs)
      res.json({
        msg: 'No Labs Found',
      });
    res.status(200).json({
      data: labs,
    });
  });
});

//Find NearBy Labs
router.get('/find-nearby', auth, function (req, res, next) {
  labModel.find().exec(function (error, labs) {
    if (error) return res.json({ msg: 'Server Error !', error: error });
    if (!labs)
      return res.json({
        msg: 'No Labs Found',
      });
    else {
      var nearLabs = [];
      var latitude = Number(req.body.latitude);
      var longitude = Number(req.body.longitude);

      var d1 = toLatLon([latitude, longitude]);

      labs.forEach((lab) => {
        //using latlng init

        var d2 = toLatLon([Number(lab.location[0]), Number(lab.location[1])]);

        //computing distance between two latlng

        var distanceinm = headingDistanceTo(d1, d2);
        console.log(distanceinm);
        var obj = {
          lab_name: lab.lab_name,
          contact_number: lab.contact_number,
          location: lab.location,
          services: lab.services,

          lab_img: lab.lab_img,
          Distance_in_meter: distanceinm.distance,
        };
        nearLabs.push(obj);
      });

      //Slice to copy nearhospitals

      var byKm = nearLabs.slice(0);

      //sorting by lowest distance first

      byKm.sort(function (a, b) {
        return a.Distance_in_meter - b.Distance_in_meter;
      });
      return res.json({
        byKm,
      });
    }
  });
});

//ADD Lab
router.post(
  '/add',
  auth,
  upload.single('lab_img'),
  [
    check('lab_name', 'Lab Name is required').not().isEmpty(),

    check('location', 'Location is required').not().isEmpty(),
    check('services', 'Services is required').not().isEmpty(),
    check('contact_number', 'Contact Number is required').not().isEmpty(),
  ],
  function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({
        errors: errors.array(),
      });
    } else {
      //check if user is admin
      userModel.findById({ _id: req.user.user.id }).exec(function (err, user) {
        if (err) return res.json({ msg: 'Server Error !' });
        //if user role is not admin
        if (user) {
          if (user.role !== 1) {
            return res.json({
              msg: 'unauthorized Access',
            });
          } else {
            //CHECK IF Lab EXISTS
            labModel
              .findOne({ lab_name: req.body.lab_name })
              .exec(function (error, lab) {
                if (error)
                  return res.json({
                    msg: 'Server ERROR',
                  });
                if (lab) {
                  return res.json({
                    msg: 'Lab already Exists',
                  });
                } else {
                  //CLoudinary upload
                  const result = cloudinary.v2.uploader.upload(req.file.path);
                  result.then((success) => {
                    if (!lab) {
                      //Creating a new Lab
                      const newLab = new labModel({
                        lab_name: req.body.lab_name,
                        lab_img: success.url,
                        public_id_cloudinary: success.public_id,
                      });

                      //Splitting Services by comma and Looping through it

                      var services_item = req.body.services.split(',');
                      services_item.forEach((service) => {
                        newLab.services.push(service);
                      });

                      //Split number by comma

                      var numbers = req.body.contact_number.split(',');
                      numbers.forEach((number) => {
                        newLab.contact_number.push(number);
                      });

                      //Split Location by comma and pushing it to newLab
                      var loc_points = req.body.location.split(',');
                      loc_points.forEach((point) => {
                        newLab.location.push(point);
                      });

                      //Saving newLab

                      newLab.save(function (error, lab) {
                        if (error)
                          next({
                            msg: 'Server Error',
                          });
                        if (lab) {
                          res.json({
                            msg: 'Lab Added',
                            lab: lab,
                          });
                        }
                      });
                    }
                  });
                }
              });
          }
        }
      });
    }
  }
);

//MODIFY Lab DETAILS

router.put('/add/:id', auth, function (req, res, next) {
  //check if user is admin
  userModel.findById({ _id: req.user.user.id }).exec(function (err, user) {
    if (err) return res.json({ msg: 'Server Error !' });
    //if user role is not admin
    if (user) {
      if (user.role !== 1) {
        return res.json({
          msg: 'unauthorized Access',
        });
      } else {
        //Creating Empty Object for inserting update
        var update = {};
        //mapping it to update
        if (req.body.lab_name) update.lab_name = req.body.lab_name;

        //Split number by comma and add it to update
        if (req.body.contact_number) {
          var newContacts = [];
          var numbers = req.body.contact_number.split(',');
          numbers.forEach((number) => {
            newContacts.push(number);
          });
          update.contact_number = newContacts;
        }

        if (req.body.location) {
          //Split Location by comma and pushing it to update
          var loc_ = [];
          var loc_points = req.body.location.split(',');
          loc_points.forEach((point) => {
            loc_.push(point);
          });
          update.location = loc_;
        }

        if (req.body.services) {
          //Splitting Services by comma
          var services = [];
          var services_item = req.body.services.split(',');
          services_item.forEach((service) => {
            services.push(service);
          });
          update.services = services;
        }

        labModel
          .findByIdAndUpdate({ _id: req.params.id }, update)
          .exec(function (error, done) {
            if (error)
              return res.json({
                msg: 'server error',
                error: error,
              });
            else {
              res.json({
                msg: 'Updated !!',
                data: done,
              });
            }
          });
      }
    }
  });
});

//DELETE HOSPITAL BY ID

router.delete('/:id', auth, function (req, res, next) {
  labModel.findOneAndDelete({ _id: req.params.id }).exec(function (error, lab) {
    if (error) return res.status(500).json({ msg: 'Server Error !' });
    if (!lab)
      return res.json({
        msg: 'Lab doesnot exist',
      });
    else {
      console.log(lab);
      cloudinary.v2.uploader.destroy(lab.public_id_cloudinary, function (
        err,
        done
      ) {
        if (err) return res.send(err);
        else {
          res.status(200).json({
            msg: 'Deleted !',
            data: lab,
          });
        }
      });
    }
  });
});

module.exports = router;
