var express = require('express');
var router = express.Router();
var cloudinary = require('cloudinary');
var multer = require('multer');
const { check, validationResult } = require('express-validator/check');
const { headingDistanceTo, toLatLon } = require('geolocation-utils');
const icuModel = require('../models/icuModel');
const myStorage = require('../configs/multerSetup');
const auth = require('../middlewares/auth');
const userModel = require('../models/userModel');

require('dotenv').config();

// CLoudinary Config
require('../configs/cloudinary');

// Multer Storage Setup
var upload = multer({
  storage: myStorage,
});

//GET ALL ICU
router.get('/', auth, function (req, res, next) {
  icuModel.find().exec(function (error, icu) {
    if (error) return res.status(500).json({ msg: 'Server Error !' });
    if (!icu)
      res.json({
        msg: 'No ICU Found',
      });
    res.status(200).json({
      data: icu,
    });
  });
});

//Find NearBy ICU
router.get('/find-nearby', auth, function (req, res, next) {
  icuModel.find().exec(function (error, icus) {
    if (error) return res.json({ msg: 'Server Error !', error: error });
    if (!icus)
      return res.json({
        msg: 'No ICU Found',
      });
    else {
      var nearIcu = [];
      var latitude = Number(req.body.latitude);
      var longitude = Number(req.body.longitude);

      var d1 = toLatLon([latitude, longitude]);

      icus.forEach((lab) => {
        //using latlng init

        var d2 = toLatLon([Number(lab.location[0]), Number(lab.location[1])]);

        //computing distance between two latlng

        var distanceinm = headingDistanceTo(d1, d2);
        console.log(distanceinm);
        var obj = {
          icu_name: lab.pharmacy_name,
          contact_number: lab.contact_number,
          location: lab.location,
          total_beds: lab.total_beds,
          price: lab.price,
          icu_img: lab.icu_img,
          Distance_in_meter: distanceinm.distance,
        };
        nearIcu.push(obj);
      });

      //Slice to copy near Icu

      var byKm = nearIcu.slice(0);

      //sorting by lowest distance first

      byKm.sort(function (a, b) {
        return a.Distance_in_meter - b.Distance_in_meter;
      });
      return res.json({
        byKm,
      });
    }
  });
});

//ADD ICU
router.post(
  '/add',
  auth,
  upload.single('icu_img'),
  [
    check('icu_name', 'ICU Name is required').not().isEmpty(),
    check('total_beds', 'Total No of Beds Available is required')
      .not()
      .isEmpty(),
    check('location', 'Location is required').not().isEmpty(),
    check('price', 'Price is required').not().isEmpty(),
    check('contact_number', 'Contact Number is required').not().isEmpty(),
  ],
  function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({
        errors: errors.array(),
      });
    } else {
      //check if user is admin
      userModel.findById({ _id: req.user.user.id }).exec(function (err, user) {
        if (err) return res.json({ msg: 'Server Error !' });
        //if user role is not admin
        if (user) {
          if (user.role !== 1) {
            return res.json({
              msg: 'unauthorized Access',
            });
          } else {
            //CHECK IF ICU EXISTS
            icuModel
              .findOne({
                icu_name: req.body.icu_name,
              })
              .exec(function (error, icu) {
                if (error)
                  return res.json({
                    msg: 'Server ERROR',
                  });
                if (icu) {
                  return res.json({
                    msg: 'ICU already Exists',
                  });
                } else {
                  //CLoudinary upload
                  const result = cloudinary.v2.uploader.upload(req.file.path);
                  result.then((success) => {
                    if (!icu) {
                      //Creating a new ICU
                      const newIcu = new icuModel({
                        icu_name: req.body.icu_name,
                        price: req.body.price,
                        icu_img: success.url,
                        total_beds: req.body.total_beds,
                        public_id_cloudinary: success.public_id,
                      });

                      //Split number by comma

                      var numbers = req.body.contact_number.split(',');
                      numbers.forEach((number) => {
                        newIcu.contact_number.push(number);
                      });

                      //Split Location by comma
                      var loc_points = req.body.location.split(',');
                      loc_points.forEach((point) => {
                        newIcu.location.push(point);
                      });

                      //Saving new ICU

                      newIcu.save(function (error, done) {
                        if (error)
                          next({
                            msg: 'Server Error',
                          });
                        if (done) {
                          res.json({
                            msg: 'ICU Added',
                            icu: done,
                          });
                        }
                      });
                    }
                  });
                }
              });
          }
        }
      });
    }
  }
);

//MODIFY ICU DETAILS

router.put('/add/:id', auth, function (req, res, next) {
  //check if user is admin
  userModel.findById({ _id: req.user.user.id }).exec(function (err, user) {
    if (err) return res.json({ msg: 'Server Error !' });
    //if user role is not admin
    if (user) {
      if (user.role !== 1) {
        return res.json({
          msg: 'unauthorized Access',
        });
      } else {
        //Creating Empty Object for inserting update
        var update = {};
        //mapping it to update
        if (req.body.icu_name) update.icu_name = req.body.icu_name;
        if (req.body.total_beds) update.total_beds = req.body.total_beds;

        //Split number by comma and add it to update
        if (req.body.contact_number) {
          var newContacts = [];
          var numbers = req.body.contact_number.split(',');
          numbers.forEach((number) => {
            newContacts.push(number);
          });
          update.contact_number = newContacts;
        }

        if (req.body.location) {
          //Split Location by comma and pushing it to update
          var loc_ = [];
          var loc_points = req.body.location.split(',');
          loc_points.forEach((point) => {
            loc_.push(point);
          });
          update.location = loc_;
        }

        if (req.body.price) update.price = req.body.price;

        icuModel
          .findByIdAndUpdate({ _id: req.params.id }, update)
          .exec(function (error, done) {
            if (error)
              return res.json({
                msg: 'server error',
                error: error,
              });
            else {
              res.json({
                msg: 'Updated !!',
                data: done,
              });
            }
          });
      }
    }
  });
});

//DELETE ICU BY ID

router.delete('/:id', auth, function (req, res, next) {
  icuModel.findOneAndDelete({ _id: req.params.id }).exec(function (error, icu) {
    if (error) return res.json({ msg: 'Server Error !' });
    if (!icu)
      return res.json({
        msg: 'ICU doesnot exist',
      });
    else {
      cloudinary.v2.uploader.destroy(icu.public_id_cloudinary, function (
        err,
        done
      ) {
        if (err) return res.send(err);
        else {
          res.status(200).json({
            msg: 'Deleted !',
            data: icu,
          });
        }
      });
    }
  });
});

module.exports = router;
