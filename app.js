var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var cloudinary = require('cloudinary');
var indexRouter = require('./controllers/index');
var apiRouter = require('./controllers/apiRoute');
var userRouter = require('./controllers/userRoute');
var doctorRouter = require('./controllers/doctorRoute');
var ambulanceRouter = require('./controllers/ambulanceRoute');
var bloodBankRouter = require('./controllers/bloobBankRoute');
var labRouter = require('./controllers/labRoute');
var pharmacyRouter = require('./controllers/pharmacyRoute');
var icuRouter = require('./controllers/icuRoute');
var helplineRouter = require('./controllers/helplineRoute.js');

require('dotenv').config();

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Database Connection
require('./connections/db');

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/hospitalapp/build'));
}

//Routes

app.use('/api', apiRouter);
app.use('/users', userRouter);
app.use('/doctors', doctorRouter);
app.use('/ambulances', ambulanceRouter);
app.use('/bloodbanks', bloodBankRouter);
app.use('/labs', labRouter);
app.use('/pharmacy', pharmacyRouter);
app.use('/icu', icuRouter);
app.use('/helpline',helplineRouter);

app.get('*', (req, res) => {
  res.sendFile('index.html');
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(process.env.PORT || 6500, function (error, done) {
  if (error) console.log('Server Listening Failed');
  else console.log('Server Listening to port ', process.env.PORT);
});
