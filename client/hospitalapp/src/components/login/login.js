import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';

export default class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  //handler for onchange event
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    //making data ready for api call
    const data = {
      username: this.state.username,
      password: this.state.password,
    };
    //making api call
    axios
      .post('https://mymernapp98.herokuapp.com/users/login', data)
      .then((response) => {
        //if token redirect to dashboard

        if (response.data.token) {
          toast.info('Logged In...');
          localStorage.setItem('token', response.data.token); //storing token in localstorage
          this.props.history.push('/dashboard');
        } else {
          toast.warn('Wrong Credentials');
        }
      })
      .catch((error) => {});
  };

  render() {
    return (
      <div>
        <h1>Please Login</h1>

        <form onSubmit={this.handleSubmit} className='form-group'>
          <label>Username</label>
          <input
            type='text'
            name='username'
            placeholder='Enter Username'
            onChange={this.handleChange}
            classname='form-control'
          />
          <label>Password</label>
          <input
            type='password'
            name='password'
            onChange={this.handleChange}
            placeholder='Enter Password'
            classname='form-control'
          />
          <button type='submit'>Login</button>
        </form>
        <Link to='/register'>Register</Link>
      </div>
    );
  }
}
