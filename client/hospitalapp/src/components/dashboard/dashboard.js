import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import AddHospital from '../hospital/addhospital/addhospital';
import AddDoctor from '../doctors/add-doctor/adddoctor';

/**
 * Dashboard Component
 */

export default class Dashboard extends Component {
  /**
   * onClick Handler for Logout button
   * Removes token from localstorage
   */
  logoutfromapp = () => {
    localStorage.clear();

    this.props.history.push('/');
  };

  render() {
    return (
      <div className='text-center'>
        <h1>Dashboard</h1>
        <button class='btn btn-danger btn-rounded' onClick={this.logoutfromapp}>
          Logout
        </button>
        <Link to='/add-ambulance' className='btn btn-success btn-sm ml-3'>
          Add Ambulance
        </Link>
        <Link to='/add-pharmacy' className='btn btn-success btn-sm ml-3'>
          Add Pharmacy
        </Link>
        <Link to='/add-bloodbank' className='btn btn-success btn-sm ml-3'>
          Add Blood Bank
        </Link>
        <Link to='/add-lab' className='btn btn-success btn-sm ml-3'>
          Add Lab
        </Link>
        <Link to='/add-ICU' className='btn btn-success btn-sm ml-3'>
          Add ICU
        </Link>
        <Link to='/add-helpline' className='btn btn-success btn-sm ml-3'>
          Add Helpline
        </Link>
        <div className='row'>
          <div className='col-6'>
            <h3>Add New Hospital</h3>
            <AddHospital />
          </div>
          <div className='col-6'>
            <h4>Add Doctor</h4>
            <AddDoctor />
          </div>
        </div>

        <Link to='/view-hospitals' className='btn btn-success mt-3 btn-sm'>
          Show Hospitals
        </Link>
        <Link to='/view-doctors' className='btn btn-warning mt-3 ml-3 btn-sm'>
          Show Doctors
        </Link>
        <Link
          to='/view-ambulances'
          className='btn btn-warning mt-3 ml-3 btn-sm'>
          Show Ambulances
        </Link>
        <Link
          to='/view-bloodbanks'
          className='btn btn-warning mt-3 ml-3 btn-sm'>
          Show Blood Banks
        </Link>
        <Link to='/view-lab' className='btn btn-warning mt-3 ml-3 btn-sm'>
          Show Labs
        </Link>
        <Link to='/view-pharmacy' className='btn btn-warning mt-3 ml-3 btn-sm'>
          Show Pharmacy
        </Link>
        <Link to='/view-ICU' className='btn btn-warning mt-3 ml-3 btn-sm'>
          Show ICU
        </Link>
        <Link to='/view-helpline' className='btn btn-warning mt-3 ml-3 btn-sm'>
          Show Helplines
        </Link>
      </div>
    );
  }
}
