import React, { Component } from 'react';
import { upload } from '../../functions/fileupload';
import { toast } from 'react-toastify';
import Select from 'react-select';
import BeautyStars from 'beauty-stars';

export default class AddHospital extends Component {
  constructor(props) {
    super();
    this.state = {
      options: [
        { value: 'Surgery', label: 'Surgery' },
        { value: 'Dermatologist', label: 'Dermatologist' },
        { value: 'Clinic', label: 'Clinic' },
      ],
      rating: null,
      selectedOptions: [],
      hospital_name: '',
      services: '',
      contact_number: '',
      location: '',
      hospital_img: '',
    };
  }

  //handler for react-select onchange event
  handleNew = (event) => {
    this.setState({
      selectedOptions: event,
    });
  };

  //handler for onchange event
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  //handler for file
  handleFile = (e) => {
    this.setState({
      hospital_img: e.target.files[0],
    });
  };

  //

  handleSubmit = (e) => {
    e.preventDefault();
    //converting selectedOptions to string (comma seprated value)
    var selectedServices = '';
    this.state.selectedOptions.forEach((element) => {
      selectedServices = selectedServices + element.value + ',';
    });

    this.setState({
      services: selectedServices,
    });
    //data object for api call
    const dataSend = {
      hospital_name: this.state.hospital_name,
      services: this.state.services,
      location: this.state.location,
      contact_number: this.state.contact_number,
      rating: this.state.rating,
    };

    //api call
    upload(
      'POST',
      'https://mymernapp98.herokuapp.com/api/',
      this.state.hospital_img,
      dataSend,
      'hospital_img'
    )
      .then((response) => {
        const res = JSON.parse(response);
        console.log(res);
        toast.success(res.msg);
        this.props.history.push('/dashboard');
      })
      .catch((error) => {});
  };

  render() {
    return (
      <div>
        <form
          className='mx-5'
          onSubmit={this.handleSubmit}
          enctype='multipart/form-data'>
          <label>Name of the Hospital</label>
          <input
            className='form-control less'
            required={true}
            onChange={this.handleChange}
            type='text'
            placeholder='Kathmandu Model Hospital'
            name='hospital_name'
          />
          <label>Services</label>

          <Select
            isMulti
            required
            name='services'
            options={this.state.options}
            onChange={this.handleNew}
            className='basic-multi-select'
            classNamePrefix='select'
          />
          <label>Location</label>
          <input
            className='form-control less'
            required={true}
            placeholder='latitude,longitude'
            onChange={this.handleChange}
            type='text'
            name='location'
          />

          <label>Contact_number</label>
          <input
            type='text'
            onChange={this.handleChange}
            className='form-control less'
            name='contact_number'
            placeholder='986765555855, 98565888'
          />
          <label>Rating :</label>
          <input
            type='text'
            className='form-control'
            onChange={this.handleChange}
            name='rating'></input>
          <div className='form-control'>
            <BeautyStars
              value={this.state.value}
              onChange={(value) => this.setState({ rating: value })}
              value={this.state.rating}
            />
          </div>

          <label>Image of Product</label>
          <input
            type='file'
            required={true}
            onChange={this.handleFile}
            className='form-control less'
            name='hospital_img'
          />
          <br></br>
          <button type='submit' className='btn btn-secondary'>
            Add Hospital
          </button>
        </form>
      </div>
    );
  }
}
