import React, { Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';

export default class ViewHospital extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
    };
  }
  componentDidMount() {
    //Fetching all hospitals by making api call
    Axios.get('https://mymernapp98.herokuapp.com/api', {
      headers: { 'x-access-token': localStorage.getItem('token') },
    }).then((hospitals) => {
      console.log(hospitals.data.data[0].hospital_name);
      this.setState({
        products: hospitals.data.data,
      });
      console.log(this.state.products);
    });
  }

  deleteProduct(id, index) {
    //show popup to confirm deletion
    const a = alert('Are you sure you want to delete?');

    //api call
    Axios.delete(`https://mymernapp98.herokuapp.com/api/${id}`, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    })
      .then((item) => {
        // updating state by removing deleted product
        const { products } = this.state;
        products.splice(index, 1);

        this.setState({
          products: products,
        });
      })
      .catch(function (error) {})
      .finally(() => {
        this.props.history.push('/view-hospitals');
      });
  }
  render() {
    return (
      <div>
        <h1>List of Added Hospitals</h1>
        <Link to='/dashboard' className='btn btn-primary mb-5'>
          Go to Dashboard
        </Link>
        <div className='row'>
          {this.state.products.map((element, i) => (
            <div
              class='card mx-2 my-2 '
              style={{ width: '10 rem', height: '10 rem' }}>
              <img
                src={element.hospital_img}
                alt='Card image cap'
                style={{ width: '250 px', height: '250px' }}
              />
              <div class='card-body'>
                <h5 class='card-title'>{element.hospital_name}</h5>
                <p>
                  Location: {element.location[0]} Latitude {element.location[1]}
                  Longitude
                </p>
                <p>
                  contact: {element.contact_number[0]}{' '}
                  {element.contact_number[1]} {element.contact_number[2]}
                </p>
                <p>
                  Services: {element.services[0]},{element.services[1]},
                  {element.services[2]}
                  {element.services[3]}
                  {element.services[4]}
                  {element.services[5]}
                </p>
                <p>Rating: {element.rating}</p>
                <Link
                  to={`/edit-hospital/${element._id}`}
                  class='btn btn-primary'>
                  Edit
                </Link>
                <button
                  to=''
                  onClick={() => this.deleteProduct(element._id, i)}
                  class='btn btn-danger'>
                  Delete
                </button>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
