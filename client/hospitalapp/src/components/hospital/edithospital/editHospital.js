import React, { Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';

export default class EditHospital extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product_id: '',
      hospital_name: '',
      services: '',
      contact_number: '',
      location: '',
      rating: '',
      hospital: {},
    };
  }

  //handler for onchange event
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  componentDidMount() {
    var product_id = this.props.match.params.id;
    this.setState({
      product_id: product_id,
    });

    //Fetching the object that matches product id
    Axios.get(`https://mymernapp98.herokuapp.com/api/hospital/${product_id}`, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    }).then((data) => {
      this.setState({
        hospital: data.data.hospital,
      });
    });
  }

  //handler for submit event
  handleSubmit = (e) => {
    e.preventDefault();
    const {
      hospital_name,
      services,
      contact_number,
      location,
      rating,
    } = this.state;
    //object ready for api call
    const datatosend = {
      hospital_name,
      services,
      contact_number,
      location,
      rating,
    };
    //APi Call

    Axios.put(
      `https://mymernapp98.herokuapp.com/api/${this.state.product_id}`,
      datatosend,
      {
        headers: { 'x-access-token': localStorage.getItem('token') },
      }
    ).then((success) => {
      console.log(success);
      toast.info('Edited !');
      this.props.history.push('/view-hospitals');
    });
  };

  render() {
    return (
      <div>
        <h1>Edit Hospital Details</h1>
        <form className='mx-5' onSubmit={this.handleSubmit}>
          <label>Name of the Hospital</label>
          <input
            className='form-control '
            required={true}
            onChange={this.handleChange}
            type='text'
            defaultValue={this.state.hospital.hospital_name}
            name='hospital_name'
          />
          <label>Services</label>
          <input
            className='form-control less '
            onChange={this.handleChange}
            type='text'
            name='services'
            defaultValue={this.state.hospital.services}
          />
          <label>Rating</label>
          <input
            className='form-control less'
            required={true}
            defaultValue={this.state.hospital.rating}
            onChange={this.handleChange}
            type='text'
            name='rating'
          />
          <label>Location</label>
          <input
            className='form-control less'
            required={true}
            defaultValue={this.state.hospital.location}
            onChange={this.handleChange}
            type='text'
            name='location'
          />

          <label>Contact_number</label>
          <input
            type='text'
            defaultValue={this.state.hospital.contact_number}
            onChange={this.handleChange}
            className='form-control less'
            name='contact_number'
          />

          <br></br>
          <button type='submit' className='btn btn-secondary'>
            Edit
          </button>
          <Link to='/view-hospitals' className='ml-3'>
            Go Back
          </Link>
        </form>
      </div>
    );
  }
}
