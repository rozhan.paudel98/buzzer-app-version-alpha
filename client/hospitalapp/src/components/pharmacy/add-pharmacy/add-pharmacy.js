import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { upload } from '../../functions/fileupload';
import { toast } from 'react-toastify';

export default class AddPharmacy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pharmacy_name: '',
      hour_open_range: '',
      contact_number: '',
      location: '',
      pharmacy_img: '',
    };
  }

  //handler for onchange event
  handleChange = (e) => {
    let { name, value, type, checked } = e.target;

    if (type === 'checkbox') {
      value = checked;
    }
    if (type === 'file') {
      value = e.target.files[0];
    }
    this.setState({
      [name]: value,
    });
  };

  //handler for onsubmit event
  handleSubmit = (e) => {
    e.preventDefault();

    const {
      pharmacy_name,
      hour_open_range,
      contact_number,
      location,
    } = this.state;

    //making object ready for api call
    const dataSend = {
      pharmacy_name,
      hour_open_range,
      contact_number,
      location,
    };
    //api call
    upload(
      'POST',
      'https://mymernapp98.herokuapp.com/pharmacy/add',
      this.state.pharmacy_img,
      dataSend,
      'pharmacy_img'
    )
      .then((response) => {
        const res = JSON.parse(response);
        console.log(response);

        toast.success(res.msg);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  render() {
    return (
      <div>
        <h1>Add Pharmacy</h1>
        <Link
          to='/dashboard'
          className='btn btn-warning mb-2 btn-circle btn-sm'>
          Go Back
        </Link>
        <form className='mx-5' onSubmit={this.handleSubmit}>
          <label>Name of Pharmacy </label>
          <input
            className='form-control less'
            required={true}
            onChange={this.handleChange}
            type='text'
            placeholder='Enter Pharmacy name'
            name='pharmacy_name'
          />

          <label>Open Hours Range </label>
          <input
            className='form-control less'
            required={true}
            onChange={this.handleChange}
            type='text'
            placeholder='8 am to 7 pm'
            name='hour_open_range'
          />

          <label>Location</label>
          <input
            className='form-control less'
            required={true}
            placeholder='latitude,longitude'
            onChange={this.handleChange}
            type='text'
            name='location'
          />

          <label>Contact_number</label>
          <input
            type='text'
            onChange={this.handleChange}
            className='form-control '
            name='contact_number'
            placeholder='986765555855, 98565888516'
          />

          <label>Pharmacy Image</label>
          <input
            type='file'
            required={true}
            onChange={this.handleChange}
            className='form-control less'
            name='pharmacy_img'
          />
          <br></br>
          <button type='submit' className='btn btn-secondary'>
            Add Pharmacy
          </button>
        </form>
      </div>
    );
  }
}
