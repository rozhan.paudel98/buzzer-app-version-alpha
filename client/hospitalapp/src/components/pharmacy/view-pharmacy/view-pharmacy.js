import React, { Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';

import './view-doctors.css';

export default class ViewPharmacy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pharmacies: [],
      pharmacy_name: '',
      hour_open_range: '',
      contact_number: '',
      location: '',
      pharmacy_id: '',
    };
  }
  componentDidMount() {
    //fetching all pharmacy and updating state
    Axios.get('https://mymernapp98.herokuapp.com/pharmacy', {
      headers: { 'x-access-token': localStorage.getItem('token') },
    }).then((data) => {
      this.setState({
        pharmacies: data.data.data,
      });
    });
  }

  deleteProduct(id, index) {
    console.log(id);
    //show popup to confirm deletion
    const a = alert('Are you sure you want to delete?');
    //api call
    Axios.delete(`https://mymernapp98.herokuapp.com/pharmacy/${id}`, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    })
      .then((item) => {
        // updating state by removing deleted pharmacy
        const { pharmacies } = this.state;
        pharmacies.splice(index, 1);
        this.setState({
          pharmacies: pharmacies,
        });
        toast.info('Pharmacy Deleted !');
      })
      .catch(function (error) {})
      .finally(() => {});
  }

  handleEdit = (id, i) => {
    //filtering lab that matches id
    var mainLab = this.state.pharmacies.filter(function (lab) {
      return lab._id == id;
    });

    //updating state

    this.setState(
      {
        pharmacy_name: mainLab[0].pharmacy_name,
        contact_number: mainLab[0].contact_number.toString(),
        location: mainLab[0].location.toString(),
        hour_open_range: mainLab[0].hour_open_range,
        pharmacy_id: id,
      },
      () => {
        console.log('done');
      }
    );
  };

  //onchange event handler for file
  handleChange = (e) => {
    let { name, value, type, checked } = e.target;

    if (type === 'file') {
      value = e.target.files[0];
      console.log(value);
    }
    this.setState({
      [name]: value,
    });
  };

  setInitialValue = () => {
    this.setState(
      {
        lab_name: '',
        services: '',
        contact_number: '',
        location: '',
      },
      () => {
        console.log('State Updated !');
      }
    );
  };

  handleSave = () => {
    //object ready for api call
    var datatoSend = {
      pharmacy_name: this.state.pharmacy_name,
      contact_number: this.state.contact_number,
      location: this.state.location,
      hour_open_range: this.state.hour_open_range,
    };

    var url_is = `https://mymernapp98.herokuapp.com/pharmacy/add/${this.state.pharmacy_id}`;
    //API CALL
    Axios.put(url_is, datatoSend, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    })
      .then((data) => {
        toast.info('Updated !!');
        Axios.get('https://mymernapp98.herokuapp.com/pharmacy', {
          headers: { 'x-access-token': localStorage.getItem('token') },
        }).then((data) => {
          this.setState({
            pharmacies: data.data.data,
          });
        });
      })
      .catch((err) => {
        console.log('error is', err);
      })
      .finally(() => {
        this.props.history.push('/view-pharmacy');
      });
  };

  render() {
    return (
      <>
        <h1 className='text-center'>Pharmacy Available </h1>
        <Link
          to='/dashboard'
          className='btn btn-warning mb-2 btn-circle btn-sm'>
          Go Back
        </Link>
        <div className='view-doc'>
          <div className=' row center mx-auto'>
            {this.state.pharmacies.map((element, i) => (
              <div key={i}>
                <div
                  className='card '
                  style={{
                    width: '250px',

                    justifyContent: 'center',
                  }}>
                  <img
                    className='card-img-top'
                    src={element.pharmacy_img}
                    alt='Card image'
                  />
                  <div className='card-body'>
                    <h4 className='card-title'>{element.pharmacy_name}</h4>
                    <p className='card-text'>
                      Location: {element.location[0]} Latitude <br />
                      {element.location[1]} Longitude <br />
                      Contact Number:{' '}
                      {element.contact_number.map(
                        (contact) => contact + ','
                      )}{' '}
                      <br />
                      Open Hour Range : {element.hour_open_range}
                    </p>
                    <div className='row'>
                      <div>
                        {/* Button trigger modal */}
                        <a
                          type='button'
                          onClick={() => this.handleEdit(element._id, i)}
                          className='btn btn-warning mt-3 btn-sm '
                          data-toggle='modal'
                          data-target='#exampleModal'>
                          Edit
                        </a>
                        {/* Modal */}
                        <div
                          className='modal fade'
                          id='exampleModal'
                          tabIndex={-1}
                          role='dialog'
                          aria-labelledby='exampleModalLabel'
                          aria-hidden='true'>
                          <div className='modal-dialog' role='document'>
                            <div className='modal-content'>
                              <div className='modal-header'>
                                <h5
                                  className='modal-title'
                                  id='exampleModalLabel'>
                                  Edit Pharmacy
                                </h5>
                                <button
                                  type='button'
                                  className='close'
                                  data-dismiss='modal'
                                  aria-label='Close'>
                                  <span
                                    onClick={this.setInitialValue}
                                    aria-hidden='true'>
                                    ×
                                  </span>
                                </button>
                              </div>
                              <div className='modal-body '>
                                <form className='mx-5'>
                                  <label>Name of Lab </label>
                                  <input
                                    className='form-control less'
                                    required={true}
                                    onChange={this.handleChange}
                                    type='text'
                                    value={this.state.pharmacy_name}
                                    name='pharmacy_name'
                                  />
                                  <label>Open Hour Range</label>

                                  <input
                                    className='form-control less'
                                    required={true}
                                    value={this.state.hour_open_range}
                                    onChange={this.handleChange}
                                    type='text'
                                    name='hour_open_range'
                                  />

                                  <label>Location</label>
                                  <input
                                    className='form-control less'
                                    required={true}
                                    value={this.state.location}
                                    onChange={this.handleChange}
                                    type='text'
                                    name='location'
                                  />

                                  <label>Contact_number</label>
                                  <input
                                    type='text'
                                    onChange={this.handleChange}
                                    className='form-control less'
                                    name='contact_number'
                                    value={this.state.contact_number}
                                  />
                                </form>
                              </div>
                              <div className='modal-footer'>
                                <button
                                  type='button'
                                  className='btn btn-secondary btn-sm'
                                  onClick={this.setInitialValue}
                                  data-dismiss='modal'>
                                  Close
                                </button>
                                <button
                                  type='button'
                                  onClick={() => this.handleSave()}
                                  className='btn btn-warning btn-sm '>
                                  Save changes
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <button
                        href='#'
                        onClick={() => this.deleteProduct(element._id, i)}
                        className='btn btn-danger ml-2 btn-sm'>
                        Delete
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </>
    );
  }
}
