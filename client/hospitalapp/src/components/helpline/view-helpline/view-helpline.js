import React, { Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';

import './view-doctors.css';

/**
 * View Helpline Component
 *  show available helplines and provides ui for edit and delete for each helpline
 */

export default class ViewHelpLine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      helplines: [],
      title: '',
      contact_number: '',
      helpline_id: '',
    };
  }
  /**
   * Fetch available helplines data and store it to state called helplines (array)
   */
  componentDidMount() {
    Axios.get('https://mymernapp98.herokuapp.com/helpline', {
      headers: { 'x-access-token': localStorage.getItem('token') },
    }).then((data) => {
      console.log(data.data.data);
      this.setState({
        helplines: data.data.data,
      });
    });
  }

  /**
   * Deletes product that matches the id by making api call and deletes the product (helpline) that matches the index from state
   *
   * @param  {string} id id of a product
   * @param  {number} index  its index in array
   */
  deleteProduct(id, index) {
    console.log(id);
    //show popup to confirm deletion
    const a = alert('Are you sure you want to delete?');

    Axios.delete(`https://mymernapp98.herokuapp.com/helpline/${id}`, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    })
      .then((item) => {
        const { helplines } = this.state;
        helplines.splice(index, 1);
        this.setState(
          {
            helplines: helplines,
          },
          () => {
            console.log('Up');
          }
        );
        toast.info('Helpline Deleted !');
        this.props.history.push('/view-helpline');
      })
      .catch(function (error) {})
      .finally(() => {});
  }

  /**
   * Filter the object helpline that matches the id and set their properties to state
   * @param  {string} id id of an helpline
   * @param  {number} i  its index(positon in array)
   */
  handleEdit = (id, i) => {
    var mainLab = this.state.helplines.filter(function (lab) {
      return lab._id == id;
    });
    this.setState(
      {
        title: mainLab[0].title,
        contact_number: mainLab[0].contact_number,
        helpline_id: id,
      },
      () => {
        console.log('done');
      }
    );
  };

  /**
   * handles the onchange event that takes place in form
   * @param  {object} e event object while onchange event
   */
  handleChange = (e) => {
    let { name, value, type, checked } = e.target;

    if (type === 'file') {
      value = e.target.files[0];
      console.log(value);
    }
    this.setState({
      [name]: value,
    });
  };

  /**
   * Sets state value to blank
   */
  setInitialValue = () => {
    this.setState(
      {
        title: '',
        contact_number: '',
      },
      () => {
        console.log('State Updated !');
      }
    );
  };

  /**
   * Basic Handler when user clicks save changes button after edit
   * This function grabs updated form fields and makes api call for edit request
   */
  handleSave = () => {
    var datatoSend = {
      title: this.state.title,
      contact_number: this.state.contact_number,
    };

    var url_is = `https://mymernapp98.herokuapp.com/helpline/add/${this.state.helpline_id}`;
    //API CALL
    Axios.put(url_is, datatoSend, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    })
      .then((data) => {
        toast.info('Updated !!');
        Axios.get('https://mymernapp98.herokuapp.com/helpline', {
          headers: { 'x-access-token': localStorage.getItem('token') },
        }).then((data) => {
          this.setState({
            helplines: data.data.data,
          });
        });
      })
      .catch((err) => {
        console.log('error is', err);
      })
      .finally(() => {
        this.props.history.push('/view-helpline');
      });
  };

  render() {
    return (
      <>
        <h1 className='text-center'>Helpline Available </h1>
        <Link
          to='/dashboard'
          className='btn btn-warning mb-2 btn-circle btn-sm'>
          Go Back
        </Link>
        <div className='view-doc'>
          <div className=' row center mx-auto'>
            {this.state.helplines.map((element, i) => (
              <div key={i}>
                <div
                  className='card card_design '
                  style={{
                    width: '250px',

                    justifyContent: 'center',
                  }}>
                  <div className='card-body'>
                    <h4 className='card-title'>{element.title}</h4>
                    <p className='card-text'>
                      CONTACT NUMBER : {element.contact_number}
                    </p>
                    <div className='row'>
                      <div>
                        {/* Button trigger modal */}
                        <a
                          type='button'
                          onClick={() => this.handleEdit(element._id, i)}
                          className='btn btn-warning mt-3 btn-sm '
                          data-toggle='modal'
                          data-target='#exampleModal'>
                          Edit
                        </a>
                        {/* Modal */}
                        <div
                          className='modal fade'
                          id='exampleModal'
                          tabIndex={-1}
                          role='dialog'
                          aria-labelledby='exampleModalLabel'
                          aria-hidden='true'>
                          <div className='modal-dialog' role='document'>
                            <div className='modal-content'>
                              <div className='modal-header'>
                                <h5
                                  className='modal-title'
                                  id='exampleModalLabel'>
                                  Edit Helpline
                                </h5>
                                <button
                                  type='button'
                                  className='close'
                                  data-dismiss='modal'
                                  aria-label='Close'>
                                  <span
                                    onClick={this.setInitialValue}
                                    aria-hidden='true'>
                                    ×
                                  </span>
                                </button>
                              </div>
                              <div className='modal-body '>
                                <form className='mx-5'>
                                  <label>Title </label>
                                  <input
                                    className='form-control'
                                    required={true}
                                    onChange={this.handleChange}
                                    type='text'
                                    value={this.state.title}
                                    name='title'
                                  />

                                  <label>Contact_number</label>
                                  <input
                                    type='text'
                                    onChange={this.handleChange}
                                    className='form-control'
                                    name='contact_number'
                                    value={this.state.contact_number}
                                  />
                                </form>
                              </div>
                              <div className='modal-footer'>
                                <button
                                  type='button'
                                  className='btn btn-secondary btn-sm'
                                  onClick={this.setInitialValue}
                                  data-dismiss='modal'>
                                  Close
                                </button>
                                <button
                                  type='button'
                                  onClick={() => this.handleSave()}
                                  className='btn btn-warning btn-sm '>
                                  Save changes
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <button
                        href='#'
                        onClick={() => this.deleteProduct(element._id, i)}
                        className='btn btn-danger ml-2 btn-sm'>
                        Delete
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </>
    );
  }
}
