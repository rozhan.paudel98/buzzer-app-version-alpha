import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { upload } from '../../functions/fileupload';
import axios from 'axios';
import { toast } from 'react-toastify';

export default class AddHelpLine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      contact_number: '',
    };
  }
  /**
   * handles the onchange event on form
   */
  handleChange = (e) => {
    let { name, value, type, checked } = e.target;

    this.setState({
      [name]: value,
    });
  };

  /**
   * handler for onsubmit event and makes api call
   *
   */

  handleSubmit = (e) => {
    e.preventDefault();

    const dataSend = {
      ...this.state,
    };

    axios
      .post('https://mymernapp98.herokuapp.com/helpline/add', dataSend, {
        headers: { 'x-access-token': localStorage.getItem('token') },
      })
      .then((data) => {
        toast.warning('Helpline Added');
        this.props.history.push('/dashboard');
      })
      .catch((error) => {
        toast.warning("Couldn't add helpline ! Contact System Admin");
      });
  };
  render() {
    return (
      <div>
        <h2>Add Helpline</h2>
        <Link to='/dashboard' className='btn btn-primary btn-sm'>
          {' '}
          Go Back
        </Link>
        <form className='mx-5' onSubmit={this.handleSubmit}>
          <label>Title for Helpline</label>
          <input
            className='form-control'
            required={true}
            onChange={this.handleChange}
            type='text'
            placeholder='Enter title '
            name='title'
          />

          <label>Contact Number</label>
          <input
            type='text'
            onChange={this.handleChange}
            className='form-control '
            name='contact_number'
            placeholder='+977-967683715'
          />

          <button type='submit' className='btn btn-secondary'>
            Add Helpline
          </button>
        </form>
      </div>
    );
  }
}
