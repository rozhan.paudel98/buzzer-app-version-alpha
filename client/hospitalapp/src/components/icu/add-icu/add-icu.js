import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { upload } from '../../functions/fileupload';
import { toast } from 'react-toastify';

export default class AddICU extends Component {
  constructor(props) {
    super(props);
    this.state = {
      icu_name: '',
      price: '',
      contact_number: '',
      location: '',
      icu_img: '',
      total_beds: '',
    };
  }

  //handler for onchange event
  handleChange = (e) => {
    let { name, value, type, checked } = e.target;

    if (type === 'checkbox') {
      value = checked;
    }
    if (type === 'file') {
      value = e.target.files[0];
    }
    this.setState({
      [name]: value,
    });
  };

  //handler for onsubmit event
  handleSubmit = (e) => {
    e.preventDefault();
    const {
      icu_name,
      price,
      contact_number,
      location,
      total_beds,
    } = this.state;

    //data ready for api call
    const dataSend = {
      icu_name,
      price,
      contact_number,
      location,
      total_beds,
    };

    //APi call
    upload(
      'POST',
      'https://mymernapp98.herokuapp.com/icu/add',
      this.state.icu_img,
      dataSend,
      'icu_img'
    )
      .then((response) => {
        const res = JSON.parse(response);
        console.log(response);

        toast.success(res.msg);
        this.props.history.push('/dashboard');
      })
      .catch((error) => {
        console.log(error);
      });
  };
  render() {
    return (
      <div>
        <h1>Add ICU</h1>
        <Link
          to='/dashboard'
          className='btn btn-warning mb-2 btn-circle btn-sm'>
          Go Back
        </Link>
        <form className='mx-5' onSubmit={this.handleSubmit}>
          <label>Name of ICU </label>
          <input
            className='form-control less'
            required={true}
            onChange={this.handleChange}
            type='text'
            placeholder='Enter ICU name'
            name='icu_name'
          />

          <label>Price </label>
          <input
            className='form-control less'
            required={true}
            onChange={this.handleChange}
            type='number'
            placeholder='Enter in Rupees'
            name='price'
          />

          <label>Location</label>
          <input
            className='form-control less'
            required={true}
            placeholder='latitude,longitude'
            onChange={this.handleChange}
            type='text'
            name='location'
          />

          <label>Contact_number</label>
          <input
            type='text'
            onChange={this.handleChange}
            className='form-control '
            name='contact_number'
            placeholder='986765555855, 98565888516'
          />
          <label>Total Beds</label>
          <input
            type='number'
            onChange={this.handleChange}
            className='form-control '
            name='total_beds'
            placeholder='Enter total available beds in ICU'
          />

          <label>ICU Image</label>
          <input
            type='file'
            required={true}
            onChange={this.handleChange}
            className='form-control less'
            name='icu_img'
          />
          <br></br>
          <button type='submit' className='btn btn-secondary'>
            Add ICU
          </button>
        </form>
      </div>
    );
  }
}
