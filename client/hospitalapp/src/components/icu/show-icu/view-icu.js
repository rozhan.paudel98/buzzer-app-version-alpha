import React, { Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';

import './view-doctors.css';

export default class ViewICU extends Component {
  constructor(props) {
    super(props);
    this.state = {
      icus: [],
      icu_name: '',
      price: '',
      contact_number: '',
      location: '',
      total_beds: '',
      icu_id: '',
    };
  }

  componentDidMount() {
    //fetching all icu
    Axios.get('https://mymernapp98.herokuapp.com/icu', {
      headers: { 'x-access-token': localStorage.getItem('token') },
    }).then((data) => {
      //storing array of icus to state
      this.setState({
        icus: data.data.data,
      });
    });
  }

  deleteProduct(id, index) {
    console.log(id);
    //show popup to confirm deletion
    const a = alert('Are you sure you want to delete?');

    //api call

    Axios.delete(`https://mymernapp98.herokuapp.com/icu/${id}`, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    })
      .then((item) => {
        const { icus } = this.state;
        icus.splice(index, 1); //removing deleted item from state
        this.setState(
          {
            icus: icus,
          },
          () => {
            console.log('Up');
          }
        );
        toast.info('ICU Deleted !');
        this.props.history.push('/view-ICU');
      })
      .catch(function (error) {})
      .finally(() => {});
  }

  handleEdit = (id, i) => {
    //getting lab that matches id
    var mainLab = this.state.icus.filter(function (lab) {
      return lab._id == id;
    });
    this.setState(
      {
        icu_name: mainLab[0].icu_name,
        contact_number: mainLab[0].contact_number.toString(),
        location: mainLab[0].location.toString(),
        price: mainLab[0].price,
        icu_id: id,
        total_beds: mainLab[0].total_beds,
      },
      () => {
        console.log('done');
      }
    );
  };

  //handler for onchange event
  handleChange = (e) => {
    let { name, value, type, checked } = e.target;

    if (type === 'file') {
      value = e.target.files[0];
      console.log(value);
    }
    this.setState({
      [name]: value,
    });
  };

  setInitialValue = () => {
    this.setState(
      {
        icu_name: '',
        price: '',
        contact_number: '',
        location: '',
        total_beds: '',
      },
      () => {
        console.log('State Updated !');
      }
    );
  };

  handleSave = () => {
    //object ready for api call

    var datatoSend = {
      icu_name: this.state.icu_name,
      contact_number: this.state.contact_number,
      location: this.state.location,
      price: this.state.price,
      total_beds: this.state.total_beds,
    };

    var url_is = `https://mymernapp98.herokuapp.com/icu/add/${this.state.icu_id}`;
    //API CALL
    Axios.put(url_is, datatoSend, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    })
      .then((data) => {
        toast.info('Updated !!');
        Axios.get('https://mymernapp98.herokuapp.com/icu', {
          headers: { 'x-access-token': localStorage.getItem('token') },
        }).then((data) => {
          this.setState({
            icus: data.data.data,
          });
        });
      })
      .catch((err) => {
        console.log('error is', err);
      })
      .finally(() => {
        this.props.history.push('/view-ICU');
      });
  };

  render() {
    return (
      <>
        <h1 className='text-center'>ICU Available </h1>
        <Link
          to='/dashboard'
          className='btn btn-warning mb-2 btn-circle btn-sm'>
          Go Back
        </Link>
        <div className='view-doc'>
          <div className=' row center mx-auto'>
            {this.state.icus.map((element, i) => (
              <div key={i}>
                <div
                  className='card card_design '
                  style={{
                    width: '250px',

                    justifyContent: 'center',
                  }}>
                  <img
                    className='card-img-top'
                    src={element.icu_img}
                    alt='Card image'
                  />
                  <div className='card-body'>
                    <h4 className='card-title'>{element.icu_name}</h4>
                    <p className='card-text'>
                      Location: {element.location[0]} Latitude <br />
                      {element.location[1]} Longitude <br />
                      Contact Number:{' '}
                      {element.contact_number.map(
                        (contact) => contact + ','
                      )}{' '}
                      <br />
                      Total Beds Available: {element.total_beds}
                      Price (per Bed): {element.price}
                    </p>
                    <div className='row'>
                      <div>
                        {/* Button trigger modal */}
                        <a
                          type='button'
                          onClick={() => this.handleEdit(element._id, i)}
                          className='btn btn-warning mt-3 btn-sm '
                          data-toggle='modal'
                          data-target='#exampleModal'>
                          Edit
                        </a>
                        {/* Modal */}
                        <div
                          className='modal fade'
                          id='exampleModal'
                          tabIndex={-1}
                          role='dialog'
                          aria-labelledby='exampleModalLabel'
                          aria-hidden='true'>
                          <div className='modal-dialog' role='document'>
                            <div className='modal-content'>
                              <div className='modal-header'>
                                <h5
                                  className='modal-title'
                                  id='exampleModalLabel'>
                                  Edit ICU
                                </h5>
                                <button
                                  type='button'
                                  className='close'
                                  data-dismiss='modal'
                                  aria-label='Close'>
                                  <span
                                    onClick={this.setInitialValue}
                                    aria-hidden='true'>
                                    ×
                                  </span>
                                </button>
                              </div>
                              <div className='modal-body '>
                                <form className='mx-5'>
                                  <label>Name of ICU </label>
                                  <input
                                    className='form-control less'
                                    required={true}
                                    onChange={this.handleChange}
                                    type='text'
                                    value={this.state.icu_name}
                                    name='icu_name'
                                  />
                                  <label>Price</label>

                                  <input
                                    className='form-control less'
                                    required={true}
                                    value={this.state.price}
                                    onChange={this.handleChange}
                                    placeholder='price per Bed'
                                    type='number'
                                    name='price'
                                  />

                                  <label>Location</label>
                                  <input
                                    className='form-control less'
                                    required={true}
                                    value={this.state.location}
                                    onChange={this.handleChange}
                                    type='text'
                                    name='location'
                                  />

                                  <label>Total Beds (Available) </label>
                                  <input
                                    type='number'
                                    onChange={this.handleChange}
                                    className='form-control less'
                                    name='total_beds'
                                    value={this.state.total_beds}
                                  />
                                  <label>Contact_number</label>
                                  <input
                                    type='text'
                                    onChange={this.handleChange}
                                    className='form-control less'
                                    name='contact_number'
                                    value={this.state.contact_number}
                                  />
                                </form>
                              </div>
                              <div className='modal-footer'>
                                <button
                                  type='button'
                                  className='btn btn-secondary btn-sm'
                                  onClick={this.setInitialValue}
                                  data-dismiss='modal'>
                                  Close
                                </button>
                                <button
                                  type='button'
                                  onClick={() => this.handleSave()}
                                  className='btn btn-warning btn-sm '>
                                  Save changes
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <button
                        href='#'
                        onClick={() => this.deleteProduct(element._id, i)}
                        className='btn btn-danger ml-2 btn-sm'>
                        Delete
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </>
    );
  }
}
