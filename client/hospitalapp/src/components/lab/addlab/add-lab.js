import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import Select from 'react-select';
import { upload } from '../../functions/fileupload';

export default class AddLab extends Component {
  constructor(props) {
    super();
    this.state = {
      options: [
        { value: 'Blood Test', label: 'Blood Test' },
        { value: 'MRI', label: 'MRI' },
        { value: 'Thyroid Test', label: 'Thyroid Test' },
      ],
      selectedOptions: [],
      lab_name: '',
      services: '',
      contact_number: '',
      location: '',
      lab_img: '',
    };
  }
  handleNew = (event) => {
    //handle for react select

    this.setState({
      selectedOptions: event,
    });
  };

  handleChange = (event) => {
    console.log(event.target.value);
    //handling form
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleFile = (e) => {
    //handling file
    this.setState({
      lab_img: e.target.files[0],
    });
    console.log(this.state);
  };

  handleSubmit = (e) => {
    e.preventDefault();

    //converting selectedOptions's value to string (comma separated)
    var selectedServices = '';
    this.state.selectedOptions.forEach((element) => {
      selectedServices = selectedServices + element.value + ',';
    });

    this.setState(
      {
        services: selectedServices,
      },
      () => {
        console.log('Update Completed!');
      }
    );
    // object ready for api call
    const dataSend = {
      lab_name: this.state.lab_name,
      services: this.state.services,
      location: this.state.location,
      contact_number: this.state.contact_number,
    };
    //api call
    upload(
      'POST',
      'https://mymernapp98.herokuapp.com/labs/add',
      this.state.lab_img,
      dataSend,
      'lab_img'
    )
      .then((response) => {
        const res = JSON.parse(response);
        toast.info(res.msg);
      })
      .catch((error) => {
        toast.info('Error occured ! Contact System Admin');
      });
  };

  render() {
    return (
      <div>
        <h2>Add Lab</h2>
        <Link to='/dashboard' className='btn btn-primary btn-sm'>
          {' '}
          Go Back
        </Link>
        <form className='mx-5' onSubmit={this.handleSubmit}>
          <label>Name of the Lab</label>
          <input
            className='form-control '
            required={true}
            name='lab_name'
            onChange={this.handleChange}
            type='text'
            placeholder='Enter Lab Name'
          />
          <label>Services</label>

          <Select
            isMulti
            required
            name='services'
            options={this.state.options}
            onChange={this.handleNew}
            className='basic-multi-select'
            classNamePrefix='select'
          />
          <label>Location</label>
          <input
            className='form-control '
            required={true}
            placeholder='Latitude,Longitude'
            onChange={this.handleChange}
            type='text'
            name='location'
          />

          <label>Contact_number</label>
          <input
            type='text'
            onChange={this.handleChange}
            className='form-control '
            name='contact_number'
            placeholder='986765555855, 01456789'
          />

          <label>Image of Lab</label>
          <input
            type='file'
            required={true}
            onChange={this.handleFile}
            className='form-control'
            name='lab_img'
          />
          <br></br>
          <button type='submit' className='btn btn-secondary btn-sm'>
            Add Lab
          </button>
        </form>
      </div>
    );
  }
}
