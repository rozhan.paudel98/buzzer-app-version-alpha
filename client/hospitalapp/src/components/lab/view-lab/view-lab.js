import React, { Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';

import './view-doctors.css';

export default class ViewLabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      labs: [],
      lab_name: '',
      services: '',
      contact_number: '',
      location: '',
      lab_id: '',
    };
  }
  componentDidMount() {
    //fetching all labs and storing it in state
    Axios.get('https://mymernapp98.herokuapp.com/labs', {
      headers: { 'x-access-token': localStorage.getItem('token') },
    }).then((data) => {
      console.log(data.data.data);
      this.setState({
        labs: data.data.data,
      });
    });
  }

  deleteProduct(id, index) {
    console.log(id);
    //show popup to confirm deletion
    const a = alert('Are you sure you want to delete?');

    //api call
    Axios.delete(`https://mymernapp98.herokuapp.com/labs/${id}`, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    })
      .then((item) => {
        //updating state by removing deleted item
        const { labs } = this.state;
        labs.splice(index, 1);
        this.setState({
          labs: labs,
        });
        toast.info('Lab Deleted !');
      })
      .catch(function (error) {})
      .finally(() => {});
  }

  handleEdit = (id, i) => {
    this.setInitialValue();

    //filtering lab that matches the id
    var mainLab = this.state.labs.filter(function (lab) {
      return lab._id == id;
    });
    this.setState(
      {
        lab_name: mainLab[0].lab_name,
        contact_number: mainLab[0].contact_number.toString(),
        location: mainLab[0].location.toString(),
        services: mainLab[0].services.toString(),
        lab_id: id,
      },
      () => {
        console.log('done');
      }
    );
  };

  //onchange event handler
  handleChange = (e) => {
    let { name, value, type, checked } = e.target;

    if (type === 'file') {
      value = e.target.files[0];
      console.log(value);
    }
    this.setState({
      [name]: value,
    });
  };

  //setting up state value to empty
  setInitialValue = () => {
    this.setState(
      {
        lab_name: '',
        services: '',
        contact_number: '',
        location: '',
      },
      () => {
        console.log('State Updated !');
      }
    );
  };

  handleSave = () => {
    //object ready for api call
    var datatoSend = {
      lab_name: this.state.lab_name,
      contact_number: this.state.contact_number,
      location: this.state.location,
      services: this.state.services,
    };
    this.setInitialValue();

    var url_is = `https://mymernapp98.herokuapp.com/labs/add/${this.state.lab_id}`;

    //API CALL
    Axios.put(url_is, datatoSend, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    })
      .then((data) => {
        toast.info('Updated !!');
        Axios.get('https://mymernapp98.herokuapp.com/labs', {
          headers: { 'x-access-token': localStorage.getItem('token') },
        }).then((data) => {
          this.setState({
            labs: data.data.data,
          });
        });
      })
      .catch((err) => {
        console.log('error is', err);
      })
      .finally(() => {
        this.props.history.push('/view-lab');
      });
  };

  render() {
    return (
      <>
        <h1 className='text-center'>Labs Available </h1>
        <Link
          to='/dashboard'
          className='btn btn-warning mb-2 btn-circle btn-sm'>
          Go Back
        </Link>
        <div className='view-doc'>
          <div className=' row center mx-auto'>
            {this.state.labs.map((element, i) => (
              <div key={i}>
                <div
                  className='card '
                  style={{
                    width: '250px',

                    justifyContent: 'center',
                  }}>
                  <img
                    className='card-img-top'
                    src={element.lab_img}
                    alt='Card image'
                  />
                  <div className='card-body'>
                    <h4 className='card-title'>{element.lab_name}</h4>
                    <p className='card-text'>
                      Location: {element.location[0]} Latitude <br />
                      {element.location[1]} Longitude <br />
                      Contact Number:{' '}
                      {element.contact_number.map(
                        (contact) => contact + ','
                      )}{' '}
                      <br />
                      Services:{' '}
                      {element.services.map((service) => service + ',')}
                    </p>
                    <div className='row'>
                      <div>
                        {/* Button trigger modal */}
                        <a
                          type='button'
                          onClick={() => this.handleEdit(element._id, i)}
                          className='btn btn-warning mt-3 btn-sm '
                          data-toggle='modal'
                          data-target='#exampleModal'>
                          Edit
                        </a>
                        {/* Modal */}
                        <div
                          className='modal fade'
                          id='exampleModal'
                          tabIndex={-1}
                          role='dialog'
                          aria-labelledby='exampleModalLabel'
                          aria-hidden='true'>
                          <div className='modal-dialog' role='document'>
                            <div className='modal-content'>
                              <div className='modal-header'>
                                <h5
                                  className='modal-title'
                                  id='exampleModalLabel'>
                                  Edit Lab
                                </h5>
                                <button
                                  type='button'
                                  className='close'
                                  data-dismiss='modal'
                                  aria-label='Close'>
                                  <span
                                    onClick={this.setInitialValue}
                                    aria-hidden='true'>
                                    ×
                                  </span>
                                </button>
                              </div>
                              <div className='modal-body '>
                                <form className='mx-5'>
                                  <label>Name of Lab </label>
                                  <input
                                    className='form-control less'
                                    required={true}
                                    onChange={this.handleChange}
                                    type='text'
                                    value={this.state.lab_name}
                                    name='lab_name'
                                  />
                                  <label>Services</label>

                                  <input
                                    className='form-control less'
                                    required={true}
                                    value={this.state.services}
                                    onChange={this.handleChange}
                                    type='text'
                                    name='services'
                                  />

                                  <label>Location</label>
                                  <input
                                    className='form-control less'
                                    required={true}
                                    value={this.state.location}
                                    onChange={this.handleChange}
                                    type='text'
                                    name='location'
                                  />

                                  <label>Contact_number</label>
                                  <input
                                    type='text'
                                    onChange={this.handleChange}
                                    className='form-control less'
                                    name='contact_number'
                                    value={this.state.contact_number}
                                  />
                                </form>
                              </div>
                              <div className='modal-footer'>
                                <button
                                  type='button'
                                  className='btn btn-secondary btn-sm'
                                  onClick={this.setInitialValue}
                                  data-dismiss='modal'>
                                  Close
                                </button>
                                <button
                                  type='button'
                                  onClick={() => this.handleSave()}
                                  className='btn btn-warning btn-sm '>
                                  Save changes
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <button
                        href='#'
                        onClick={() => this.deleteProduct(element._id, i)}
                        className='btn btn-danger ml-2 btn-sm'>
                        Delete
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </>
    );
  }
}
