import React, { Component } from 'react';
import Axios from 'axios';
import { toast } from 'react-toastify';
export default class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      role: '',
    };
  }

  //onchange event handler for form
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  //onsubmit handler for form
  handleSubmit = (e) => {
    e.preventDefault();

    // making api call to register user
    Axios.post('https://mymernapp98.herokuapp.com/users/', {
      username: this.state.username,
      password: this.state.password,
      role: this.state.role,
    }).then((success) => {
      toast.info(success.data.msg);
      this.props.history.push('/login');
    });
  };

  render() {
    return (
      <div>
        <h1>Register</h1>

        <form onSubmit={this.handleSubmit}>
          <label>Username</label>
          <input
            type='text'
            name='username'
            placeholder='Enter Username'
            onChange={this.handleChange}
            classname='form-control'
          />
          <label>Password</label>
          <input
            type='password'
            onChange={this.handleChange}
            name='password'
            placeholder='Enter Password'
            classname='form-control'
          />
          <label>Role</label>
          <input
            type='text'
            onChange={this.handleChange}
            name='role'
            placeholder='1 for admin 2 for Normal User'
            classname='form-control'
          />

          <button type='submit'>Register</button>
        </form>
      </div>
    );
  }
}
