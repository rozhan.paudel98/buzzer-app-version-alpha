import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { upload } from '../../functions/fileupload';
import { toast } from 'react-toastify';
/**
 * Add Ambulance Component
 */

export default class AddAmbulance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ambulance_name: '',
      contact_number: '',
      location: '',
      ambulance_img: '',
      rate: '',
    };
  }
  /**
   * handler for onchange event
   * @param  {object} e event
   *
   */
  handleChange = (e) => {
    let { name, value, type, checked } = e.target;
    if (type === 'file') {
      value = e.target.files[0];
    }
    this.setState({
      [name]: value,
    });
  };

  /**
   * handler for onsubmit event
   * makes an api call to add ambulance
   * @param  {object} e Event
   */
  handleSubmit = (e) => {
    e.preventDefault();
    const { ambulance_name, contact_number, rate, location } = this.state;

    const dataSend = {
      ambulance_name,
      contact_number,
      rate,
      location,
    };

    upload(
      'POST',
      'https://mymernapp98.herokuapp.com/ambulances/add',
      this.state.ambulance_img,
      dataSend,
      'ambulance_img'
    )
      .then((response) => {
        const res = JSON.parse(response);

        toast.success(res.msg);
        //todo
        this.props.history.push('/dashboard');
      })
      .catch((error) => {
        console.log(error);
      });
  };
  render() {
    return (
      <div>
        <h2>Add Ambulance</h2>
        <Link to='/dashboard' className='btn btn-primary btn-sm'>
          {' '}
          Go Back
        </Link>
        <form
          className='mx-5'
          onSubmit={this.handleSubmit}
          enctype='multipart/form-data'>
          <label>Name </label>
          <input
            className='form-control'
            required={true}
            onChange={this.handleChange}
            type='text'
            placeholder='Enter Ambulance Name'
            name='ambulance_name'
          />

          <label>Location</label>
          <input
            className='form-control '
            required={true}
            placeholder='latitude,longitude'
            onChange={this.handleChange}
            type='text'
            name='location'
          />

          <label>Contact_number</label>
          <input
            type='text'
            onChange={this.handleChange}
            className='form-control '
            name='contact_number'
            placeholder='986765555855, 98565888516'
          />
          <label>Rate Per Service (Rupees) :</label>
          <input
            type='number'
            onChange={this.handleChange}
            className='form-control '
            name='rate'
            placeholder='500'
          />

          <label>Ambulance Image</label>
          <input
            type='file'
            required={true}
            onChange={this.handleChange}
            className='form-control'
            name='ambulance_img'
          />
          <br></br>
          <button type='submit' className='btn btn-secondary'>
            Add Ambulance
          </button>
        </form>
      </div>
    );
  }
}
