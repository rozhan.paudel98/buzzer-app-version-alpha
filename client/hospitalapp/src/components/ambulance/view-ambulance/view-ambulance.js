import React, { Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import './view-doctors.css';

/**
 * shows list of ambulances with functionality like deletion and modification
 */

export default class ViewAmbulances extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ambulances: [],
      ambulance_name: '',
      contact_number: '',
      location: '',
      rate: '',
      ambulance_id: '',
    };
  }

  /**
   * Fetch all ambulances details and store it to state
   */
  componentDidMount() {
    Axios.get('https://mymernapp98.herokuapp.com/ambulances').then((data) => {
      console.log(data.data);
      this.setState({
        ambulances: data.data,
      });
    });
  }

  /**
   * deleteProduct
   * deletes product that matches with id
   * @param  {string} id id of product
   * @param  {number} index index of product in array
   */
  deleteProduct(id, index) {
    const a = alert('Are you sure you want to delete?');

    Axios.delete(`https://mymernapp98.herokuapp.com/ambulances/${id}`, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    })
      .then((item) => {
        const { ambulances } = this.state;
        ambulances.splice(index, 1);
        this.setState({
          ambulances: ambulances,
        });
        toast.info('Doctor Deleted !');
      })
      .catch(function (error) {})
      .finally(() => {
        this.props.history.push('/view-ambulances');
      });
  }

  /**
   * HandleEdit
   * Filter the object that matches the id and set their property value to state
   * @param  {string} id id of an helpline
   * @param  {number} i  its index(positon in array)
   */
  handleEdit = (id, i) => {
    //Filtering Ambulance
    console.log(id);
    var mainAmbulance = this.state.ambulances.filter(function (ambulance) {
      return ambulance._id == id;
    });

    this.setState({
      ambulance_name: mainAmbulance[0].ambulance_name,

      contact_number: mainAmbulance[0].contact_number.toString(),
      location: mainAmbulance[0].location.toString(),
      rate: mainAmbulance[0].rate,
      ambulance_id: id,
    });
  };

  /**
   * handler for onchange event
   * @param {object} e event
   */
  handleChange = (e) => {
    let { name, value, type, checked } = e.target;

    this.setState({
      [name]: value,
    });
  };

  /**
   * Basic Handler when user clicks save changes button after edit
   * This function grabs updated form fields and makes api call
   */
  handleSave = () => {
    var datatoSend = {
      ambulance_name: this.state.ambulance_name,
      contact_number: this.state.contact_number,
      location: this.state.location,
      rate: this.state.rate,
    };

    var url_is = `https://mymernapp98.herokuapp.com/ambulances/add/${this.state.ambulance_id}`;
    //API CALL

    Axios.put(url_is, datatoSend, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    })
      .then((data) => {
        this.setState({
          ambulance_name: '',
          contact_number: '',
          location: '',
          rate: '',
        });
        toast.info('Updated !!');
        this.props.history.push('/view-ambulances');
      })
      .catch((err) => {
        console.log('error is', err);
      });
  };

  componentDidUpdate() {
    Axios.get('https://mymernapp98.herokuapp.com/ambulances').then((data) => {
      this.setState({
        ambulances: data.data,
      });
    });
  }

  render() {
    return (
      <>
        <h1 className='text-center'>Ambulances Available </h1>
        <Link
          to='/dashboard'
          className='btn btn-warning mb-2 btn-circle btn-sm'>
          Go Back
        </Link>
        <div className='view-doc'>
          <div class=' row center mx-auto'>
            {this.state.ambulances.map((element, i) => (
              <div>
                <div
                  class='card '
                  style={{
                    width: '250px',

                    justifyContent: 'center',
                  }}>
                  <img
                    class='card-img-top'
                    src={element.ambulance_img}
                    alt='Card image'
                  />
                  <div class='card-body'>
                    <h4 class='card-title'>{element.ambulance_name}</h4>
                    <p class='card-text'>
                      Location: {element.location[0]} Latitude <br />
                      {element.location[1]} Longitude <br />
                      Contact Number: {element.contact_number[0]},
                      {element.contact_number[1]} <br />
                      Rate Per Service: {element.rate} <br />
                    </p>
                    <div className='row'>
                      <div>
                        {/* Button trigger modal */}
                        <a
                          type='button'
                          onClick={() => this.handleEdit(element._id, i)}
                          className='btn btn-warning mt-3 btn-sm '
                          data-toggle='modal'
                          data-target='#exampleModal'>
                          Edit
                        </a>
                        {/* Modal */}
                        <div
                          className='modal fade'
                          id='exampleModal'
                          tabIndex={-1}
                          role='dialog'
                          aria-labelledby='exampleModalLabel'
                          aria-hidden='true'>
                          <div className='modal-dialog' role='document'>
                            <div className='modal-content'>
                              <div className='modal-header'>
                                <h5
                                  className='modal-title'
                                  id='exampleModalLabel'>
                                  Edit Ambulance
                                </h5>
                                <button
                                  type='button'
                                  className='close'
                                  data-dismiss='modal'
                                  aria-label='Close'>
                                  <span aria-hidden='true'>×</span>
                                </button>
                              </div>
                              <div className='modal-body '>
                                <form
                                  className='mx-5'
                                  enctype='multipart/form-data'>
                                  <label>Name </label>
                                  <input
                                    className='form-control'
                                    required={true}
                                    onChange={this.handleChange}
                                    type='text'
                                    defaultValue={this.state.ambulance_name}
                                    name='ambulance_name'
                                  />

                                  <label>Location</label>
                                  <input
                                    className='form-control'
                                    required={true}
                                    defaultValue={this.state.location}
                                    onChange={this.handleChange}
                                    type='text'
                                    name='location'
                                  />

                                  <label>Contact_number</label>
                                  <input
                                    type='text'
                                    onChange={this.handleChange}
                                    className='form-control'
                                    name='contact_number'
                                    defaultValue={this.state.contact_number}
                                  />
                                  <label>Rate (Per Service) :</label>
                                  <input
                                    type='number'
                                    onChange={this.handleChange}
                                    className='form-control'
                                    name='rate'
                                    defaultValue={this.state.rate}
                                  />
                                </form>
                              </div>
                              <div className='modal-footer'>
                                <button
                                  type='button'
                                  className='btn btn-secondary btn-sm'
                                  data-dismiss='modal'>
                                  Close
                                </button>
                                <button
                                  type='button'
                                  onClick={() => this.handleSave()}
                                  className='btn btn-warning btn-sm '>
                                  Save changes
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <button
                        href='#'
                        onClick={() => this.deleteProduct(element._id, i)}
                        class='btn btn-danger ml-2 btn-sm'>
                        Delete
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </>
    );
  }
}
