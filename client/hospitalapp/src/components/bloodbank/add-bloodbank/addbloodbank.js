import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { upload } from '../../functions/fileupload';
import axios from 'axios';
import { toast } from 'react-toastify';

export default class AddBloodBank extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bloodbank_name: '',
      contact_number: '',
      location: '',
    };
  }

  //handler for onchange event for form fields

  handleChange = (e) => {
    let { name, value, type, checked } = e.target;

    this.setState({
      [name]: value,
    });
  };

  // handler for onsubmit event
  // makes api call

  handleSubmit = (e) => {
    e.preventDefault();
    const { bloodbank_name, contact_number, location } = this.state;

    const dataSend = {
      bloodbank_name,
      contact_number,
      location,
    };
    //api call
    axios
      .post('https://mymernapp98.herokuapp.com/bloodbanks/add', dataSend, {
        headers: { 'x-access-token': localStorage.getItem('token') },
      })
      .then((data) => {
        console.log(data);
        toast.warning('Blood Bank Added');
        this.props.history.push('/dashboard');
      })
      .catch((error) => {
        toast.warning("Couldn't add blood bank ! Contact System Admin");
      });
  };
  render() {
    return (
      <div>
        <h2>Add Blood Bank</h2>
        <Link to='/dashboard' className='btn btn-primary btn-sm'>
          {' '}
          Go Back
        </Link>
        <form className='mx-5' onSubmit={this.handleSubmit}>
          <label>Name of Blood Bank</label>
          <input
            className='form-control'
            required={true}
            onChange={this.handleChange}
            type='text'
            placeholder='Enter Blood Bank Name'
            name='bloodbank_name'
          />

          <label>Location</label>
          <input
            className='form-control '
            required={true}
            placeholder='latitude,longitude'
            onChange={this.handleChange}
            type='text'
            name='location'
          />

          <label>Contact Number</label>
          <input
            type='text'
            onChange={this.handleChange}
            className='form-control '
            name='contact_number'
            placeholder='986765555855, 98565888516'
          />

          <button type='submit' className='btn btn-secondary'>
            Add Blood Bank
          </button>
        </form>
      </div>
    );
  }
}
