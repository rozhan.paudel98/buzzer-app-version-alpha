import React, { Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import './view-doctors.css';

export default class ViewBloodBank extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bloodbanks: [],
      bloodbank_name: '',
      contact_number: '',
      location: '',
      bloodbank_id: '',
    };
  }

  //Fetch data and store it to state
  componentDidMount() {
    Axios.get('https://mymernapp98.herokuapp.com/bloodbanks').then((data) => {
      this.setState({
        bloodbanks: data.data,
      });
    });
  }

  /**
   * Deletes product that matches the id
   * @param  {string} id id of an helpline
   * @param  {number} i  its index(positon in array)
   */
  deleteProduct(id, index) {
    const a = alert('Are you sure you want to delete?');

    Axios.delete(`https://mymernapp98.herokuapp.com/bloodbanks/${id}`, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    })
      .then((item) => {
        const { bloodbanks } = this.state;
        //updating state by removing deleted item
        bloodbanks.splice(index, 1);
        this.setState({
          bloodbanks: bloodbanks,
        });
        toast.info('Doctor Deleted !');
      })
      .catch(function (error) {})
      .finally(() => {
        this.props.history.push('/view-bloodbanks');
      });
  }

  handleEdit = (id, i) => {
    //Filtering Blood Bank

    var mainBloodBank = this.state.bloodbanks.filter(function (bloodbank) {
      return bloodbank._id == id;
    });
    // setting state
    this.setState({
      bloodbank_name: mainBloodBank[0].bloodbank_name,

      contact_number: mainBloodBank[0].contact_number.toString(),
      location: mainBloodBank[0].location.toString(),
      bloodbank_id: id,
    });
  };

  //Handling the changes in form
  handleChange = (e) => {
    let { name, value, type, checked } = e.target;

    this.setState({
      [name]: value,
    });
  };

  //when user click save changes button
  handleSave = () => {
    var datatoSend = {
      bloodbank_name: this.state.bloodbank_name,
      contact_number: this.state.contact_number,
      location: this.state.location,
    };

    var url_is = `https://mymernapp98.herokuapp.com/bloodbanks/add/${this.state.bloodbank_id}`;
    //API CALL

    Axios.put(url_is, datatoSend, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    })
      .then((data) => {
        //Updating the state again
        Axios.get('https://mymernapp98.herokuapp.com/bloodbanks').then(
          (data) => {
            console.log('data is ', data.data);
            this.setState({
              bloodbanks: data.data,
              bloodbank_name: '',
              contact_number: '',
              location: '',
            });
          }
        );
        toast.info('Updated !!');
        this.props.history.push('/view-bloodbanks');
      })
      .catch((err) => {
        console.log('error is', err);
      });
  };

  render() {
    return (
      <>
        <h1 className='text-center'>Blood Banks Available </h1>
        <Link
          to='/dashboard'
          className='btn btn-warning mb-2 btn-circle btn-sm'>
          Go Back
        </Link>
        <div className='view-doc'>
          <div class=' row center mx-auto'>
            {this.state.bloodbanks.map((element, i) => (
              <div>
                <div
                  class='card '
                  style={{
                    width: '250px',

                    justifyContent: 'center',
                  }}>
                  <div class='card-body'>
                    <h4 class='card-title'>{element.bloodbank_name}</h4>
                    <p class='card-text'>
                      Location: {element.location[0]} Latitude <br />
                      {element.location[1]} Longitude <br />
                      Contact Number: {element.contact_number[0]},
                      {element.contact_number[1]} <br />
                    </p>
                    <div className='row'>
                      <div>
                        {/* Button trigger modal */}
                        <a
                          type='button'
                          onClick={() => this.handleEdit(element._id, i)}
                          className='btn btn-warning mt-3 btn-sm '
                          data-toggle='modal'
                          data-target='#exampleModal'>
                          Edit
                        </a>
                        {/* Modal */}
                        <div
                          className='modal fade'
                          id='exampleModal'
                          tabIndex={-1}
                          role='dialog'
                          aria-labelledby='exampleModalLabel'
                          aria-hidden='true'>
                          <div className='modal-dialog' role='document'>
                            <div className='modal-content'>
                              <div className='modal-header'>
                                <h5
                                  className='modal-title'
                                  id='exampleModalLabel'>
                                  Edit Blood Bank
                                </h5>
                                <button
                                  type='button'
                                  className='close'
                                  data-dismiss='modal'
                                  aria-label='Close'>
                                  <span aria-hidden='true'>×</span>
                                </button>
                              </div>
                              <div className='modal-body '>
                                <form
                                  className='mx-5'
                                  enctype='multipart/form-data'>
                                  <label>Name of Blood Bank </label>
                                  <input
                                    className='form-control'
                                    required={true}
                                    onChange={this.handleChange}
                                    type='text'
                                    value={this.state.bloodbank_name}
                                    name='bloodbank_name'
                                  />

                                  <label>Location</label>
                                  <input
                                    className='form-control'
                                    required={true}
                                    value={this.state.location}
                                    onChange={this.handleChange}
                                    type='text'
                                    name='location'
                                  />

                                  <label>Contact_number</label>
                                  <input
                                    type='text'
                                    onChange={this.handleChange}
                                    className='form-control'
                                    name='contact_number'
                                    value={this.state.contact_number}
                                  />
                                </form>
                              </div>
                              <div className='modal-footer'>
                                <button
                                  type='button'
                                  className='btn btn-secondary btn-sm'
                                  data-dismiss='modal'>
                                  Close
                                </button>
                                <button
                                  type='button'
                                  onClick={() => this.handleSave()}
                                  className='btn btn-warning btn-sm '>
                                  Save changes
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <button
                        href='#'
                        onClick={() => this.deleteProduct(element._id, i)}
                        class='btn btn-danger ml-2 btn-sm'>
                        Delete
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </>
    );
  }
}
