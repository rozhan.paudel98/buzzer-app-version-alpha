import React, { Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';

import BeautyStars from 'beauty-stars';
import './view-doctors.css';

export default class ViewDoctors extends Component {
  constructor(props) {
    super(props);
    this.state = {
      doctors: [],
      doctor_name: '',
      nmc_number: '',
      contact_number: '',
      location: '',
      rating: '',
      speciality: '',
      person_id: '',
    };
  }

  //fetch all doctors
  componentDidMount() {
    Axios.get('https://mymernapp98.herokuapp.com/doctors').then((data) => {
      //store it to state
      this.setState({
        doctors: data.data,
      });
    });
  }

  deleteProduct(id, index) {
    console.log(id);
    //show popup to confirm deletion
    const a = alert('Are you sure you want to delete?');
    //api call
    Axios.delete(`https://mymernapp98.herokuapp.com/doctors/${id}`, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    })
      .then((item) => {
        const { doctors } = this.state;
        //removing deleted doctor
        doctors.splice(index, 1);
        this.setState({
          doctors: doctors,
        });
        toast.info('Doctor Deleted !');
      })
      .catch(function (error) {})
      .finally(() => {
        this.props.history.push('/view-doctors');
      });
  }

  handleEdit = (id, i) => {
    //filtering doctor that matches id
    var maindoctor = this.state.doctors.filter(function (doctor) {
      return doctor._id == id;
    });

    this.setState({
      doctor_name: maindoctor[0].doctor_name,
      nmc_number: maindoctor[0].nmc_number,
      contact_number: maindoctor[0].contact_number.toString(),
      location: maindoctor[0].location.toString(),
      rating: maindoctor[0].rating,
      speciality: maindoctor[0].speciality,
      person_id: id,
    });
  };

  //onchange handler for form fields
  handleChange = (e) => {
    let { name, value, type, checked } = e.target;

    if (type === 'checkbox') {
      value = checked;
    }
    if (type === 'file') {
      value = e.target.files[0];
      console.log(value);
    }
    this.setState({
      [name]: value,
    });
  };

  //handler for speciality
  handleSelect = (event) => {
    this.setState({
      speciality: event.target.value,
    });
  };

  //handler for save changes button
  handleSave = (id, i) => {
    var datatoSend = {
      doctor_name: this.state.doctor_name,
      nmc_number: this.state.nmc_number,
      contact_number: this.state.contact_number,
      location: this.state.location,
      speciality: this.state.speciality,
      rating: this.state.rating,
    };

    var url_is = `https://mymernapp98.herokuapp.com/doctors/add/${this.state.person_id}`;
    //API CALL
    Axios.put(url_is, datatoSend, {
      headers: { 'x-access-token': localStorage.getItem('token') },
    })
      .then((data) => {
        console.log(data);
        //setting blank to state
        this.setState({
          doctor_name: '',
          nmc_number: '',
          contact_number: '',
          location: '',

          rating: '',
          speciality: '',
        });
        toast.info('Updated !!');
        this.props.history.push('/view-doctors');
      })
      .catch((err) => {
        console.log('error is', err);
      });
  };
  componentDidUpdate() {
    //updating state
    Axios.get('https://mymernapp98.herokuapp.com/doctors').then((data) => {
      this.setState({
        doctors: data.data,
      });
    });
  }

  render() {
    return (
      <>
        <h1 className='text-center'>Doctors Available </h1>
        <Link
          to='/dashboard'
          className='btn btn-warning mb-2 btn-circle btn-sm'>
          Go Back
        </Link>
        <div className='view-doc'>
          <div class=' row center mx-auto'>
            {this.state.doctors.map((element, i) => (
              <div>
                <div
                  class='card '
                  style={{
                    width: '250px',

                    justifyContent: 'center',
                  }}>
                  <img
                    class='card-img-top'
                    src={element.doctor_img}
                    alt='Card image'
                  />
                  <div class='card-body'>
                    <h4 class='card-title'>{element.doctor_name}</h4>
                    <p class='card-text'>
                      Nmc Number: {element.nmc_number} <br />
                      Location: {element.location[0]} Latitude <br />
                      {element.location[1]} Longitude <br />
                      Contact Number: {element.contact_number} <br />
                      Speciality:{element.speciality} <br />
                      Rating: {element.rating} <br />
                    </p>
                    <div className='row'>
                      <div>
                        {/* Button trigger modal */}
                        <a
                          type='button'
                          onClick={() => this.handleEdit(element._id, i)}
                          className='btn btn-warning mt-3 btn-sm '
                          data-toggle='modal'
                          data-target='#exampleModal'>
                          Edit
                        </a>
                        {/* Modal */}
                        <div
                          className='modal fade'
                          id='exampleModal'
                          tabIndex={-1}
                          role='dialog'
                          aria-labelledby='exampleModalLabel'
                          aria-hidden='true'>
                          <div className='modal-dialog' role='document'>
                            <div className='modal-content'>
                              <div className='modal-header'>
                                <h5
                                  className='modal-title'
                                  id='exampleModalLabel'>
                                  Edit Doctor
                                </h5>
                                <button
                                  type='button'
                                  className='close'
                                  data-dismiss='modal'
                                  aria-label='Close'>
                                  <span aria-hidden='true'>×</span>
                                </button>
                              </div>
                              <div className='modal-body '>
                                <form
                                  className='mx-5'
                                  enctype='multipart/form-data'>
                                  <label>Name </label>
                                  <input
                                    className='form-control less'
                                    required={true}
                                    onChange={this.handleChange}
                                    type='text'
                                    defaultValue={this.state.doctor_name}
                                    name='doctor_name'
                                  />
                                  <label>Speciality</label>

                                  <select
                                    className='form-control'
                                    required={true}
                                    value={this.state.speciality}
                                    onChange={this.handleSelect}
                                    // defaultValue={this.state.speciality}
                                    name='speciality'
                                    id='speciality'>
                                    <option value=''>Select Speciality</option>
                                    <option value='Dermatologist'>
                                      Dermatologist{' '}
                                    </option>
                                    <option value='Pathology'>Pathology</option>
                                    <option value='Oncology'>Oncology</option>
                                    <option value='Pediatrics'>
                                      pediatrics
                                    </option>
                                  </select>

                                  <label>NMC Number </label>
                                  <input
                                    className='form-control less'
                                    required={true}
                                    onChange={this.handleChange}
                                    type='text'
                                    defaultValue={this.state.nmc_number}
                                    name='nmc_number'
                                  />

                                  <label>Location</label>
                                  <input
                                    className='form-control less'
                                    required={true}
                                    defaultValue={this.state.location}
                                    onChange={this.handleChange}
                                    type='text'
                                    name='location'
                                  />

                                  <label>Contact_number</label>
                                  <input
                                    type='text'
                                    onChange={this.handleChange}
                                    className='form-control less'
                                    name='contact_number'
                                    defaultValue={this.state.contact_number}
                                  />
                                  <label>Rating :</label>

                                  <div className='form-control'>
                                    <BeautyStars
                                      onChange={(value) =>
                                        this.setState({ rating: value })
                                      }
                                      value={this.state.rating}
                                    />
                                  </div>
                                </form>
                              </div>
                              <div className='modal-footer'>
                                <button
                                  type='button'
                                  className='btn btn-secondary btn-sm'
                                  data-dismiss='modal'>
                                  Close
                                </button>
                                <button
                                  type='button'
                                  onClick={() =>
                                    this.handleSave(element._id, i)
                                  }
                                  className='btn btn-warning btn-sm '>
                                  Save changes
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <button
                        href='#'
                        onClick={() => this.deleteProduct(element._id, i)}
                        class='btn btn-danger ml-2 btn-sm'>
                        Delete
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </>
    );
  }
}
