import React, { Component } from 'react';
import { upload } from '../../functions/fileupload';
import { toast } from 'react-toastify';

import BeautyStars from 'beauty-stars';

export default class AddDoctor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      doctor_name: '',
      nmc_number: '',
      contact_number: '',
      location: '',
      doctor_img: '',
      rating: '',
      speciality: '',
    };
  }

  //onchange handler
  handleChange = (e) => {
    let { name, value, type, checked } = e.target;

    if (type === 'checkbox') {
      value = checked;
    }
    if (type === 'file') {
      value = e.target.files[0];
      console.log(value);
    }
    this.setState({
      [name]: value,
    });
  };

  //onchange handler for speciality
  handleSelect = (event) => {
    this.setState({
      speciality: event.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    const {
      doctor_name,
      nmc_number,
      contact_number,
      speciality,
      rating,
      location,
    } = this.state;

    const dataSend = {
      doctor_name,
      nmc_number,
      contact_number,
      speciality,
      rating,
      location,
    };

    //Api Call
    upload(
      'POST',
      'https://mymernapp98.herokuapp.com/doctors/add',
      this.state.doctor_img,
      dataSend,
      'doctor_img'
    )
      .then((response) => {
        const res = JSON.parse(response);

        toast.success(res.msg);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  render() {
    return (
      <div>
        <form
          className='mx-5'
          onSubmit={this.handleSubmit}
          enctype='multipart/form-data'>
          <label>Name </label>
          <input
            className='form-control less'
            required={true}
            onChange={this.handleChange}
            type='text'
            placeholder='Enter Doctor name'
            name='doctor_name'
          />
          <label>Speciality</label>

          <select
            className='form-control'
            required={true}
            onChange={this.handleSelect}
            name='speciality'
            id='speciality'>
            <option value=''>Select Speciality</option>
            <option value='Dermatologist'>Dermatologist </option>
            <option value='Pathology'>Pathology</option>
            <option value='Oncology'>Oncology</option>
            <option value='Pediatrics'>pediatrics</option>
          </select>

          <label>NMC Number </label>
          <input
            className='form-control less'
            required={true}
            onChange={this.handleChange}
            type='text'
            placeholder='Enter Nmc Number'
            name='nmc_number'
          />

          <label>Location</label>
          <input
            className='form-control less'
            required={true}
            placeholder='latitude,longitude'
            onChange={this.handleChange}
            type='text'
            name='location'
          />

          <label>Contact_number</label>
          <input
            type='text'
            onChange={this.handleChange}
            className='form-control less'
            name='contact_number'
            placeholder='986765555855, 98565888516'
          />
          <label>Rating :</label>

          <div className='form-control'>
            <BeautyStars
              value={this.state.value}
              onChange={(value) => this.setState({ rating: value })}
              value={this.state.rating}
            />
          </div>

          <label>Doctor Image</label>
          <input
            type='file'
            required={true}
            onChange={this.handleChange}
            className='form-control less'
            name='doctor_img'
          />
          <br></br>
          <button type='submit' className='btn btn-secondary'>
            Add Doctor
          </button>
        </form>
      </div>
    );
  }
}
