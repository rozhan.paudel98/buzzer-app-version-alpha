import React from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import ViewHospital from './components/hospital/viewhospitals/viewhospital';
import EditHospital from './components/hospital/edithospital/editHospital';
import ViewDoctors from './components/doctors/view-doctors/view-doctors';
import LoginForm from './components/login/login';
import RegisterForm from './components/register/register';
import NotFound from './components/notfound/notfound';
import Dashboard from './components/dashboard/dashboard';
import AddAmbulance from './components/ambulance/add-ambulance/addAmbulance';
import ViewAmbulances from './components/ambulance/view-ambulance/view-ambulance';
import AddBloodBank from './components/bloodbank/add-bloodbank/addbloodbank';
import ViewBloodBank from './components/bloodbank/view-bloodbank/viewbloodbank';
import AddLab from './components/lab/addlab/add-lab';
import ViewLabs from './components/lab/view-lab/view-lab';
import AddPharmacy from './components/pharmacy/add-pharmacy/add-pharmacy';
import ViewPharmacy from './components/pharmacy/view-pharmacy/view-pharmacy';
import AddICU from './components/icu/add-icu/add-icu';
import ViewICU from './components/icu/show-icu/view-icu';
import AddHelpLine from './components/helpline/add-helpline/add-helpline';
import ViewHelpLine from './components/helpline/view-helpline/view-helpline';

const ProtectedRoute = ({ component: Component, ...props }) => {
  return (
    <Route
      {...props}
      render={
        (props) =>
          localStorage.getItem('token') ? (
            <>
              <Component {...props}></Component>
            </>
          ) : (
            <Redirect to='/'></Redirect>
          ) // TODO send props
      }
    />
  );
};

function App() {
  return (
    <div className='App container'>
      <ToastContainer />
      <Router>
        <Switch>
          <Route path='/' exact component={LoginForm} />
          <Route path='/login' component={LoginForm} />
          <Route path='/register' component={RegisterForm} />
          {/* protected routes */}
          <ProtectedRoute path='/dashboard' component={Dashboard} />
          <ProtectedRoute path='/add-ambulance' component={AddAmbulance} />
          <ProtectedRoute path='/view-hospitals' component={ViewHospital} />
          <ProtectedRoute path='/edit-hospital/:id' component={EditHospital} />
          <ProtectedRoute path='/view-doctors' component={ViewDoctors} />
          <ProtectedRoute path='/view-ambulances' component={ViewAmbulances} />
          <ProtectedRoute path='/add-bloodbank' component={AddBloodBank} />
          <ProtectedRoute path='/view-bloodbanks' component={ViewBloodBank} />
          <ProtectedRoute path='/add-lab' component={AddLab} />
          <ProtectedRoute path='/view-lab' component={ViewLabs} />
          <ProtectedRoute path='/add-pharmacy' component={AddPharmacy} />
          <ProtectedRoute path='/view-pharmacy' component={ViewPharmacy} />
          <ProtectedRoute path='/add-ICU' component={AddICU} />
          <ProtectedRoute path='/view-ICU' component={ViewICU} />
          <ProtectedRoute path='/add-helpline' component={AddHelpLine} />
          <ProtectedRoute path='/view-helpline' component={ViewHelpLine} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
